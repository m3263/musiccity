from fastapi import FastAPI, Response, status, Depends, HTTPException
from pydantic import BaseModel
from typing import Optional
import threading
import _thread

from typing import List

from sqlalchemy.orm import Session

import crud
import models
import schemas
import scrape
from connection import SessionLocal, engine, DEFAULT_SESSION_FACTORY

from starlette.middleware.cors import CORSMiddleware

models.Base.metadata.create_all(bind=engine)

# uvicorn main:app --reload
# Or
# python -m uvicorn main:app --reload

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


def get_db_threaded():
    db = DEFAULT_SESSION_FACTORY()
    try:
        yield db
    finally:
        db.close()


@app.get("/", status_code=200)
def root():
    return {"message": "Health Check"}

@app.get("/search", status_code=200)
def search(query, page: int, step: int, response: Response, db: Session = Depends(get_db)):
    if query == "":
        query = []
    else:
        query = query.split()
    response.headers["Access-Control-Allow-Origin"] = "*"
    return crud.search(db, query, page, step)

@app.get("/artist/followers", status_code=200)
def followers(limit: int, response: Response, db: Session = Depends(get_db)):
    response.headers["Access-Control-Allow-Origin"] = "*"
    return crud.get_artists_followers(db)[:limit]


@app.get("/scrapeuris", status_code=200)
def scrapeURI(db: Session = Depends(get_db_threaded)):
    _thread.start_new_thread(scrape.scrapeArtistURIS, (db,))
    return {"message": "Process Started"}


@app.get("/artist/{id}", status_code=200)
def get_artist(id, response: Response, db: Session = Depends(get_db)):
    response.headers["Access-Control-Allow-Origin"] = "*"
    return crud.read_artist(db, id)


@app.get("/artists", status_code=200)
def get_artists(
        response: Response,
        step: int, 
        page: int,
        search: Optional[str] = "",
        sort: Optional[str] = "name",
        order: Optional[str] = "asc",
        genre: Optional[str] = "",
        related: Optional[str] = "",
        db: Session = Depends(get_db),
    ):
    if genre == "":
        genre = []
    else:
        genre = genre.split(",")
    if related == "":
        related = []
    else:
        related = related.split(",")
    if search == "":
        search = []
    else:
        search = search.split()
    response.headers["Access-Control-Allow-Origin"] = "*"
    query = crud.get_all_artists(
        db, sort, order, search, genre, related
    )
    return {'count': len(query), 'results': query[(page - 1) * step: page * step]}

@app.get("/artistRel/{id}", status_code=200)
def get_artist_rels(id, response: Response, db: Session = Depends(get_db)):
    response.headers["Access-Control-Allow-Origin"] = "*"
    return crud.get_artists_associations(db, id)

@app.get("/cities")
def get_cities(
        response: Response,
        page: int,
        step: int,
        search: Optional[str] = "",
        state: Optional[str] = "",
        country: Optional[str] = "",
        sort: Optional[str] = "city",
        order: Optional[str] = "asc",
        db: Session = Depends(get_db)
    ):
    if country == "":
        country = []
    else:
        country = country.split(",")
    if state == "":
        state = []
    else:
        state = state.split(",")
    if search == "":
        search = []
    else:
        search = search.split()
    response.headers["Access-Control-Allow-Origin"] = "*"
    query =  crud.get_all_cities(db, sort, order, state, country, search)
    return {'count': len(query), 'results': query[(page - 1) * step: page * step]}

@app.get("/citiesLinks")
def get_cities(
        response: Response,
        db: Session = Depends(get_db)
    ):
    response.headers["Access-Control-Allow-Origin"] = "*"
    query =  crud.get_all_citiesLinks(db)
    # results = []
    # for city in query:
    #     print(city.id)
    #     results.append(crud.get_city_by_id(db, city.id))
    return {'count': len(query), 'results': query}

@app.get("/cities/{city_id}")
def get_city(response: Response, city_id, db: Session = Depends(get_db)):
    response.headers["Access-Control-Allow-Origin"] = "*"
    return crud.get_city_by_id(db, city_id)


# Event API Codes


@app.get("/events/{event_id}")
def get_event(event_id, db: Session = Depends(get_db)):
    return crud.read_event(db, event_id)


@app.get("/events/{event_id}/links")
def get_event_links(event_id, db: Session = Depends(get_db)):
    return crud.event_links(event_id, db)


#EVENTTODO

@app.get("/events")
def get_events_search(response: Response,
    pageSize: int,
    currPage:int,
    sort: Optional[str] = "time",
    order: Optional[str] = "asc",
    search: Optional[str] = "",
    db: Session = Depends(get_db),
):
    if search == "":
        search = []
    else:
         search = search.split()
    #sortBy = {'featured': featured, 'venue': venue, 'city': city, 'time': time, }
    currPage = int(currPage) + 1
    pageSize = int(pageSize)
    query = crud.event_search(currPage, pageSize, sort, search, order, db)
    count = 0
    if(len(query) - (currPage - 1) * pageSize > 0) and currPage > 0:
        count = len(query)
    response.headers["Access-Control-Allow-Origin"] = "*"
    return {'count': count, 'results': query[(currPage - 1) * pageSize:currPage * pageSize]}
    


@app.get("/scrapeeventuris", status_code=200)
def scrapeEventURI(db: Session = Depends(get_db_threaded)):
    bearer_token = ""
    try:
        bearer_token = get_bandsintown_token()
    except:
        return {"message": "BandInTown Token failed"}

    # threading.Thread(target=scrape.scrapeEventsURIS, args=(bearer_token, db)).start()
    return {"message": "Disabled"}


# Meta Codes
@app.get("/checkmappings", status_code=200)
def check_mappings(db: Session = Depends(get_db)):
    return crud.check_for_mappings(db)
