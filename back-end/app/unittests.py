from typing import Generator
import main
import crud
import models

import pytest

session = None


class TestBackend:
    @pytest.mark.run(order=1)
    
    # Test 1
    def test_db_connection(self):
        global session
        gen = main.get_db()
        session = next(gen)
        assert isinstance(gen, Generator)

    # Test 2
    def test_read_artist(self):
        data = crud.read_artist(session, "6eUKZXaKkcviH0Ku9w2n3V")
        assert isinstance(data, models.Artist)

    # Test 3
    def test_get_all_artists(self):
        data = crud.get_all_artists(session, "name", "asc", [], [], [])
        assert isinstance(data, list)
        assert len(data) == 105

    # Test 4
    def test_get_artists_by_search(self):
        data = crud.get_all_artists(session, "name", "asc", ["trav", "week"], [], [])
        assert isinstance(data, list)
        assert len(data) == 3

    # Test 5
    def test_get_artists_by_sort(self):
        data = crud.get_all_artists(session, "followers", "desc", [], [], [])
        assert isinstance(data, list)
        assert len(data) == 105

    # Test 6
    def test_get_artists_by_search_and_sort(self):
        data = crud.get_all_artists(session, "popularity", "asc", ["trav"], [], [])
        assert isinstance(data, list)
        assert len(data) == 3

    # Test 7
    def test_get_artists_by_search_and_sort_and_genre(self):
        data = crud.get_all_artists(session, "popularity", "asc", [], ["trap", "rap"], [])
        assert isinstance(data, list)
        assert len(data) == 7

    # Test 8
    def test_get_artists_by_search_and_sort_and_genre_and_related(self):
        data = crud.get_all_artists(session, "popularity", "asc", [], ["trap", "rap"], ["T", "A"])
        assert isinstance(data, list)
        assert len(data) == 2

    # Test 9
    def test_get_artist_relationships(self):
        data = crud.get_artists_associations(session, "6eUKZXaKkcviH0Ku9w2n3V")
        assert isinstance(data, list)
        assert "event" in data[0]
        assert "city" in data[0]

    # Test 10
    def test_get_all_cities(self):
        data = crud.get_all_cities(session, "city", "asc", ["Colorado", "England"], ["United States of America", "United Kingdom"], ["b"])
        assert isinstance(data, list)
        assert len(data) == 4

    # Test 11
    def test_get_city_by_id(self):
        boston_city = {
            'name': 'Boston', 
            'country': 'United States of America', 
            'humidity': 81, 
            'temperature': '70.84', 
            'geodb_id': 120358, 
            'image1': 'https://nsbe.fiu.edu/wp-content/uploads/2016/03/boston-skyline.jpg', 'youtube_url': 'https://www.youtube.com/watch?v=e5cm2SDuHxA', 
            'longitude': -71.0625, 
            'id': 'Q100', 
            'state': 'Massachusetts', 
            'elevation': 43, 
            'population': 692600, 
            'geodb_wiki_id': 'Q100', 
            'image2': 'https://render.fineartamerica.com/images/rendered/default/print/8.000/5.375/break/images-medium-5/boston-skyline-michael-tompsett.jpg', 
            'latitude': 42.358333333, 
            'timezone': 'America__New_York', 
        }
        data = crud.get_city_by_id(session, boston_city["id"])
        assert data 
        for key in boston_city:
            assert boston_city[key] == data[key]

    # Test 12
    def test_get_city_links(self):
        boston_city = {
            'id': 'Q100', 
            'links': [{'artist': ('Twenty One Pilots', '3YQKmKGau1PzlVlkL1iodx'), 'event': ('Paradise Rock Club', '102682738')}, {'artist': ('Twenty One Pilots', '3YQKmKGau1PzlVlkL1iodx'), 'event': ('House Of Blues Boston', '102682740')}, {'artist': ('Twenty One Pilots', '3YQKmKGau1PzlVlkL1iodx'), 'event': ('Agganis Arena', '102682743')}, {'artist': ('Twenty One Pilots', '3YQKmKGau1PzlVlkL1iodx'), 'event': ('TD Garden', '102682745')}, {'artist': ('Harry Styles', '6KImCVD70vtIoJWnq6nGn3'), 'event': ('TD Garden', '101776003')}, {'artist': ('Wesley Safadão', '1AL2GKpmRrKXkYIcASuRFa'), 'event': ('House Of Blues Boston', '1023223647')}, {'artist': ('Snoop Dogg', '7hJcb9fa4alzcOq3EaNPoG'), 'event': ('Big Night Live', '1023152936')}, {'artist': ('Rauw Alejandro', '1mcTU81TzQhprhouKaTkpq'), 'event': ('Agganis Arena', '1023202645')}, {'artist': ('Myke Towers', '7iK8PXO48WeuP03g8YR51W'), 'event': ('Boch Center - Wang Theatre', '1022594098')}]
        }
        data = crud.get_city_by_id(session, boston_city["id"])
        assert data 
        assert data["links"] == boston_city["links"]
    # Test 13
    def test_read_event(self):
        data = crud.read_event(session, "1023104702")
        assert isinstance(data, models.Event)

    # Test 14
    def test_get_all_events(self):
        data = crud.event_search(1, 50, "venue_name", ["austin", "park"], "asc", session)
        assert isinstance(data, list)

    # Test 15
    def test_get_event_links(self):
        data = crud.event_links("1023104702", session)
        assert data

    # Test 16
    def test_get_search(self):
        data = crud.search(session, "%%", 1, 10)
        assert "artists" in data
        assert "events" in data
        assert "cities" in data

    # Test 17
    def test_get_artists_followers(self):
        data = crud.get_artists_followers(session)
        assert len(data) == 105

    # Test 18
    def test_get_all_citiesLinks(self):
        data = crud.get_all_citiesLinks(session)
        assert len(data) == 418