from pandas.io import pickle
from sqlalchemy.orm import Session
from sqlalchemy.sql.expression import cast
from sqlalchemy import any_
import models
import schemas
import requests
import main
import scrape
from sqlalchemy import func, or_, String
import re


def update_artist(db: Session, data: schemas.ArtistCreate):
    db_artist = models.Artist(**data.dict())

def search(db: Session, searchString, page, step):
    artists = get_all_artists(db, "name", "asc", searchString, [], [])
    events = event_search(1, 50, "venue_name", searchString, "asc", db)
    cities = get_all_cities(db, "city", "asc", [], [], searchString)
    return {
            'artists': {
                'count': len(artists), 
                'results': artists[(page - 1) * step: page * step]
            }, 
            'events': {
                'count': len(events), 
                'results': events[(page - 1) * step: page * step], 
            },
            'cities': {
                'count': len(cities), 
                'results': cities[(page - 1) * step: page * step]
            }
        }


# Events
def create_event(
    db: Session,
    id,
    artist_name,
    artist_image_url,
    facebook_page_url,
    event_url,
    sale_time,
    date,
    description,
    title,
    country,
    venue_name,
    latitude,
    longitude,
    city,
    lineup,
):
    db_event = models.Event(
        id=id,
        artist_name=artist_name,
        artist_image_url=artist_image_url,
        facebook_page_url=facebook_page_url,
        event_url=event_url,
        sale_time=sale_time,
        date=date,
        description=description,
        title=title,
        country=country,
        venue_name=venue_name,
        latitude=latitude,
        longitude=longitude,
        city=city,
        lineup=lineup,
    )
    db.merge(db_event)
    db.commit()


def get_artists_associations(db: Session, id):
    relationships = (
        db.query(models.Associate.event_id, models.Associate.city_id)
        .filter(models.Associate.artist_id == id)
        .all()
    )
    result = []
    for row in relationships:
        data = {}
        data["event"] = db.query(models.Event).filter(models.Event.id == row[0]).first()
        data["city"] = (
            db.query(models.Cities).filter(models.Cities.id == row[1]).first()
        )
        result.append(data)
    return result

def create_artist(db: Session, id, name, image, followers, popularity, genres):
    db_artist = models.Artist(
        id=id,
        name=name,
        image=image,
        followers=followers,
        popularity=popularity,
        genres=genres,
    )
    db.merge(db_artist)
    db.commit()


def read_artist(db: Session, id):
    data = db.query(models.Artist).filter(models.Artist.id == id).first()
    return data


def formatNumber(num):
    if "." in num:
        return ""
    try:
        return "%" + str(re.findall('\d+', num )[0]) + "%"
    except:
        return ""

def get_artists_followers(db: Session):
    query = db.query(
        models.Artist.name,
        models.Artist.followers,
        models.Artist.genres
    ).order_by(models.Artist.followers.desc())
    return query.all()

def get_all_artists(db: Session, sort, order, search, genre, related):
    query = db.query(
        models.Artist.name,
        models.Artist.id,
        models.Artist.related,
        models.Artist.genres,
        models.Artist.followers,
        models.Artist.popularity,
        models.Artist.image,
    )
    searches = []
    for term in search:
        term = "%" + term + "%"
        searches.append(models.Artist.name.ilike(term))
        searches.append(func.array_to_string(models.Artist.related, ",").ilike(term))
        searches.append(func.array_to_string(models.Artist.genres, ",").ilike(term))
        searches.append(cast(models.Artist.followers/1000000, String).ilike(formatNumber(term)))
        searches.append(cast(models.Artist.popularity, String).ilike(term.replace(".", "")))

    query = query.filter(or_(*tuple(searches)))

    if len(genre) > 0:
        for i in range(len(genre)):
            query = query.filter(or_(
                func.array_to_string(models.Artist.genres, ",").ilike("%," + genre[i]),
                func.array_to_string(models.Artist.genres, ",").ilike("%," + genre[i] + ",%"),
                func.array_to_string(models.Artist.genres, ",").ilike(genre[i] + ",%"),
            ))
    if len(related) > 0:
        for i in range(len(related)):
            query = query.filter(or_(
                func.array_to_string(models.Artist.related, ",").ilike("%," + related[i] + "%"),
                func.array_to_string(models.Artist.related, ",").ilike(related[i] + "%"),
            ))
    
    if sort == 'name':
        if order == 'asc':
            query = query.order_by(models.Artist.name.asc())
        elif order == 'desc':
            query = query.order_by(models.Artist.name.desc())
    elif sort == 'popularity':
        if order == 'asc':
            query = query.order_by(models.Artist.popularity.asc())
        elif order == 'desc':
            query = query.order_by(models.Artist.popularity.desc())
    elif sort == 'followers':
        if order == 'asc':
            query = query.order_by(models.Artist.followers.asc())
        elif order == 'desc':
            query = query.order_by(models.Artist.followers.desc())
    return query.all()


# Events
def read_event(db: Session, id):
    data = db.query(models.Event).filter(models.Event.id == id).first()
    return data

def get_all_events(db: Session):
    return db.query(models.Event).all()

def event_links(event_id, db: Session):
    associates = db.query(models.Associate).filter_by(event_id=event_id).all()
    result = []
    for associate in associates:
        link_object = {}
        link_object["artist"] = (
            db.query(models.Artist.name, models.Artist.id)
            .filter_by(id=associate.artist_id)
            .first()
        )
        link_object["city"] = (
            db.query(models.Cities.name, models.Cities.id)
            .filter_by(id=associate.city_id)
            .first()
        )
        result.append(link_object)
    return result


#EVENTTODO
#Get a page's worth of event data
def events_paged(currPage, pageSize, db):
    #currPage default is 1
    data = db.query(models.Event).limit(pageSize * currPage) 
    return data.all()

#Get a page's worth of event data sorted or searched
def event_search(currPage, pageSize, sortBy, search, order, db):
    query = db.query(
        models.Event.title,
        models.Event.id,
        models.Event.venue_name,
        models.Event.country,
        models.Event.city,
        models.Event.date,
        models.Event.artist_name,
    )
    searches = []
    for term in search:
        term = "%" + term + "%"
        searches.append(models.Event.title.ilike(term))
        searches.append(models.Event.venue_name.ilike(term))
        searches.append(models.Event.country.ilike(term))
        searches.append(models.Event.city.ilike(term))
        searches.append(models.Event.date.ilike(term))
        searches.append(models.Event.artist_name.ilike(term))

    query = query.filter(or_(*tuple(searches)))
    if sortBy == 'venue_name':
        if order == 'asc':
            query = query.order_by(models.Event.venue_name.asc())
        elif order == 'desc':
            query = query.order_by(models.Event.venue_name.desc())
    elif sortBy == 'country':
        if order == 'asc':
            query = query.order_by(models.Event.country.asc())
        elif order == 'desc':
            query = query.order_by(models.Event.country.desc())
    elif sortBy == 'city':
        if order == 'asc':
            query = query.order_by(models.Event.city.asc())
        elif order == 'desc':
            query = query.order_by(models.Event.city.desc())
    elif sortBy == 'time':
        if order == 'asc':
            query = query.order_by(models.Event.date.asc())
        elif order == 'desc':
            query = query.order_by(models.Event.date.desc())
    elif sortBy == 'featured':
            if order == 'asc':
                query = query.order_by(models.Event.artist_name.asc())
            elif order == 'desc':
                query = query.order_by(models.Event.artist_name.desc())
    else:
        query = db.query(models.Event) 
    return query.all()

    # Cities
def get_all_citiesLinks(db: Session):
    query = db.query(models.Cities, models.Associate).join(models.Associate)
    return query.all()

# Cities
def get_all_cities(db: Session, sort, order, state, country, search):
    query = db.query(models.Cities)

    searches = []
    for term in search:
        term = "%" + term + "%"
        searches.append(models.Cities.name.ilike(term))
        searches.append(models.Cities.state.ilike(term))
        searches.append(models.Cities.country.ilike(term))
        searches.append(cast(models.Cities.population, String).ilike(term))
        searches.append(cast(models.Cities.temperature, String).ilike(term))

    query = query.filter(or_(*tuple(searches)))

    if len(state) > 0:
        query = query.filter(models.Cities.state.in_(state))
    if len(country) > 0:
        query = query.filter(models.Cities.country.in_(country))

    if sort == 'city':
        if order == 'asc':
            query = query.order_by(models.Cities.name.asc())
        elif order == 'desc':
            query = query.order_by(models.Cities.name.desc())
    elif sort == 'population':
        if order == 'asc':
            query = query.order_by(models.Cities.population.asc())
        elif order == 'desc':
            query = query.order_by(models.Cities.population.desc())
    elif sort == 'temperature':
        if order == 'asc':
            query = query.order_by(models.Cities.temperature.asc())
        elif order == 'desc':
            query = query.order_by(models.Cities.temperature.desc())
    return query.all()

def get_city_by_id(db: Session, city_id):
    city = db.query(models.Cities).filter_by(id=city_id).first()
    city = city.__dict__
    associates = db.query(models.Associate).filter_by(city_id=city_id).all()
    result = []
    for associate in associates:
        link_object = {}
        link_object["artist"] = (
            db.query(models.Artist.name, models.Artist.id)
            .filter_by(id=associate.artist_id)
            .first()
        )
        link_object["event"] = (
            db.query(models.Event.venue_name, models.Event.id)
            .filter_by(id=associate.event_id)
            .first()
        )
        result.append(link_object)
    city["links"] = list(result)
    return city 


# Database Meta
def check_for_mappings(db: Session, VERBOSE=False):
    artists_in_db = db.query(models.Artist.id).all()
    events_in_db = db.query(models.Event.id).all()
    cities_in_db = db.query(models.Cities.id).all()
    mappings_in_db = db.query(
        models.Associate.artist_id, models.Associate.event_id, models.Associate.city_id
    ).all()
    artists = set()
    events = set()
    cities = set()
    # Getting a set of IDs from models in database
    for art_row in artists_in_db:
        artists.add(art_row["id"])
    for event_row in events_in_db:
        events.add(event_row["id"])
    for city_row in cities_in_db:
        cities.add(city_row["id"])

    # Getting a set of IDs that we have mapped
    mapped_artists = set()
    mapped_events = set()
    mapped_cities = set()
    for maprow in mappings_in_db:
        mapped_artists.add(maprow["artist_id"])
        mapped_events.add(maprow["event_id"])
        mapped_cities.add(maprow["city_id"])

    unmapped_artists = artists - mapped_artists
    unmapped_events = events - mapped_events
    unmapped_cities = cities - mapped_cities

    # In case we want to see some stats
    if VERBOSE:
        print(
            "Artists in Database :",
            len(artists),
            " Events in Database: ",
            len(events),
            " Cities in Database :",
            len(cities),
        )
        print(
            "Artists Mapped in Database :",
            len(mapped_artists),
            " Events Mapped in Database: ",
            len(mapped_events),
            " Cities Mapped in Database :",
            len(mapped_cities),
        )
        print("Unmapped Artists:\n", unmapped_artists)
        print("Unmapped Events:\n", unmapped_events)
        print("Unmapped Cities:\n", unmapped_cities)
    unmapped_objects = {}
    unmapped_objects["unmapped_artists"] = unmapped_artists
    unmapped_objects["unmapped_events"] = unmapped_events
    unmapped_objects["unmapped_cities"] = unmapped_cities

    return unmapped_objects


"""
#For Testing Stuff

if __name__ == "__main__" :
    with scrape.session_scope() as session:
        check_for_mappings(session, True)

"""
