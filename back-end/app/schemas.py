from typing import List, Optional

from pydantic import BaseModel

# class AlbumData(BaseModel):
#     id: str
#     name: str
#     date: str
#     image: str
#     link: str


class ArtistCreate(BaseModel):
    id: str
    name: str
    image: str
    followers: int
    popularity: int
    genres: List[str]
    albums: List[str] = []
    tracks: List[str] = []
    related: List[str] = []
    songs: List[str] = []


# class ItemBase(BaseModel):
#     title: str
#     description: Optional[str] = None


# class ItemCreate(ItemBase):
#     pass


# class Item(ItemBase):
#     id: int
#     owner_id: int

#     class Config:
#         orm_mode = True


# class UserBase(BaseModel):
#     email: str


# class UserCreate(UserBase):
#     password: str

# class User(UserBase):
#     id: int
#     is_active: bool
#     items: List[Item] = []

#     class Config:
#         orm_mode = True
