from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, PickleType, Float
from sqlalchemy.dialects.postgresql import ARRAY

from sqlalchemy.orm import relationship


from connection import Base


class Artist(Base):
    __tablename__ = "artists"

    id = Column(String, primary_key=True, index=True)
    name = Column(String, index=True)
    image = Column(String)
    followers = Column(Integer)
    popularity = Column(Integer)
    genres = Column(ARRAY(String))
    albums = Column(ARRAY(String))
    tracks = Column(ARRAY(String))
    related = Column(ARRAY(String))
    songs = Column(ARRAY(String))


class Cities(Base):
    __tablename__ = "cities"

    id = Column(String, primary_key=True, index=True)
    name = Column(String, index=True)
    state = Column(String)
    country = Column(String)
    elevation = Column(Integer)
    humidity = Column(Integer)
    temperature = Column(String)
    population = Column(Integer)
    geodb_id = Column(Integer)
    geodb_wiki_id = Column(String)
    image1 = Column(String)
    image2 = Column(String)
    youtube_url = Column(String)
    latitude = Column(Float)
    longitude = Column(Float)
    timezone = Column(String)


class Event(Base):
    __tablename__ = "events"
    id = Column(String, primary_key=True, index=True)

    artist_name = Column(String)
    artist_image_url = Column(String)
    facebook_page_url = Column(String)

    event_url = Column(String)
    sale_time = Column(String)
    date = Column(String)
    description = Column(String)
    title = Column(String)
    country = Column(String)
    venue_name = Column(String)
    latitude = Column(Float)
    longitude = Column(Float)
    city = Column(String)
    lineup = Column(ARRAY(String))


class Associate(Base):
    __tablename__ = "associate"

    id = Column(Integer, primary_key=True, index=True)
    artist_id = Column(String, ForeignKey("artists.id"), index=True)
    event_id = Column(String, ForeignKey("events.id"), index=True)
    city_id = Column(String, ForeignKey("cities.id"), index=True)


#     owner = relationship("User", back_populates="items")
