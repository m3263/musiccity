from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import os
import abc
from dotenv import load_dotenv

load_dotenv()

db_url = os.getenv("SQLALCHEMY_DATABASE_URL")

engine = create_engine(db_url)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()

DEFAULT_SESSION_FACTORY = sessionmaker(
    bind=create_engine(
        db_url,
        isolation_level="REPEATABLE READ",
        echo_pool=True,
        pool_pre_ping=True,
        # echo=True,
    ),
    autoflush=False,
    autocommit=False,
)
