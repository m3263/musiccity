from sqlalchemy.orm import Session
import models
import schemas
import crud
import pandas as pd
import _thread
import requests
import threading
from contextlib import contextmanager
from connection import SessionLocal
import time
from dotenv import load_dotenv
import os
import json

load_dotenv()


def get_spotify_bearer_token():
    data = {"grant_type": "client_credentials"}
    response = requests.post(
        spotify_access_token_url,
        data=data,
        allow_redirects=False,
        auth=(spotify_client_id, spotify_client_secret),
    )
    return response.json()["access_token"]


spotify_access_token_url = (
    "https://accounts.spotify.com/api/token?grant_type=client_credentials"
)
spotify_client_id = os.getenv("spotify_client_id")
spotify_client_secret = os.getenv("spotify_client_secret")
genius_token = os.getenv("genius_token")


@contextmanager
def session_scope():
    """Provide a transactional scope around a series of operations."""
    session = SessionLocal()
    try:
        yield session
        session.commit()
    except:
        session.rollback()
        raise
    finally:
        session.close()


def update_artist_thread_worker(data: schemas.ArtistCreate, bearer_token):
    tracks = [
        json.dumps(track)
        for track in get_top_tracks(data.id, bearer_token, market="US")
    ]
    data.tracks = tracks
    songs = get_songs(data.name)
    songData = []
    for songmedia in songs:
        song = get_song(str(songmedia["result"]["id"]))
        songData.append(json.dumps(song))
    data.songs = songData
    related = [artist["name"] for artist in get_related(data.id, bearer_token)]
    data.related = related
    # We're using the session context here.
    albums = get_albums(data.id, bearer_token)
    albumData = []
    for album in albums:
        if type(album) == dict:
            image = "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/480px-No_image_available.svg.png"
            if "images" in album and len(album["images"]) > 0:
                image = album["images"][0]["url"]
            albumData.append(
                json.dumps(
                    {
                        "id": album["id"],
                        "name": album["name"],
                        "date": album["release_date"],
                        "image": image,
                        "link": album["external_urls"]["spotify"],
                    }
                )
            )
    data.albums = albumData
    with session_scope() as session:
        # time.sleep(1)
        crud.update_artist(session, data)


def scrapeArtistURIS(db: Session):
    bearer_token = ""
    try:
        bearer_token = get_spotify_bearer_token()
    except:
        return {"message": "Spotify Authentication Failed"}
    artists = pd.read_csv("artists_trunc.csv")
    artist_ids = artists["id"]
    # for artist in artists.iloc[:,1]:
    #     artist_ids.append(artist[artist.rfind(':') + 1:])

    headers = {"Authorization": "Bearer " + bearer_token}
    i = 0
    url = "https://api.spotify.com/v1/artists"
    while i < len(artist_ids):
        response = requests.get(
            url=url, headers=headers, params={"ids": ",".join(artist_ids[i : i + 50])}
        ).json()
        i += 50
        if "artists" not in response:
            print(response)
            continue
        for artist in response["artists"]:
            image = "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/480px-No_image_available.svg.png"
            if len(artist["images"]) > 0:
                image = artist["images"][0]["url"]
            data = schemas.ArtistCreate(
                id=artist["id"],
                name=artist["name"],
                image=image,
                followers=artist["followers"]["total"],
                popularity=artist["popularity"],
                genres=artist["genres"],
            )
            # update_artist_thread_worker(data)
            # threading.Thread(target=update_artist_thread_worker, args=()).start()
            time.sleep(1)
            _thread.start_new_thread(update_artist_thread_worker, (data, bearer_token))
        if i % 100 == 0:
            print(i)
        time.sleep(0.75)


# def updateArtistData(db:Session, data):
#     for artist in data:
#         image = "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/480px-No_image_available.svg.png"
#         if len(artist['images']) > 0:
#             image = artist['images'][0]['url']
#         threading.Thread(target=update_artist_thread_worker, args=(artist['id'], artist['name'], image, artist['followers']['total'], artist['popularity'], artist['genres'])).start()
#         time.sleep(0.25)

# def get_artist_ids(data):
#     ids = []
#     for item in data:
#         if "album" in item:
#             if "artists" in item['album']:
#                 ids.append(item['album']['artists'][0]["id"])
#         if "artists" in item:
#             ids.append(item['artists'][0]["id"])
#     return ids

# def get_all_artists(data, bearer_token):
#     artist_ids = get_artist_ids(data)
#     headers = {'Authorization': 'Bearer ' + bearer_token}
#     result = []
#     i = 0
#     url = "https://api.spotify.com/v1/artists"
#     while i < len(artist_ids):
#         response = requests.get(url=url, headers=headers, params={"ids": ",".join(artist_ids[i:i + 50])}).json()
#         i += 50
#         result += response['artists']
#     return result

# def get_artists(artist, album, track, genre, bearer_token, limit=50, offset=0):
#     headers = {'Authorization': 'Bearer ' + bearer_token}
#     url = "https://api.spotify.com/v1/search"
#     query = artist
#     if artist != "":
#         query += ' '
#     query +=  'genre:"' + genre + '" album:"' + album + '" track:"' + track + '"'
#     typeparam = "artist"
#     if album != "":
#         typeparam += ",album"
#     if track != "":
#         typeparam += ",track"
#     params = {"type": typeparam, "q": query, "limit": limit, "offset": offset}
#     data = requests.get(url=url, headers=headers, params=params).json()
#     artists = []
#     if "artists" in data:
#         artists = data['artists']['items']
#     if "albums" in data:
#         albums = data['albums']['items']
#         artists += get_all_artists(albums, bearer_token)
#     if "tracks" in data:
#         tracks = data['tracks']['items']
#         artists += get_all_artists(tracks, bearer_token)
#     return artists

# def get_all_artists_data(bearer_token, db, artist="", album="", track="", genre=""):
#     try:
#         artists = []
#         data = get_artists(artist, album, track, genre, bearer_token, 50, 0)
#         i = 50
#         artists += data
#         while len(data) == 50:
#             data = get_artists(artist, album, track, genre, bearer_token, 50, i)
#             artists += data
#             i += 50
#         threading.Thread(target=updateArtistData, args=(db, artists)).start()
#         return artists
#     except Exception as e:
#         return str(e)


def get_songs(artist):
    try:
        url = "https://api.genius.com/search"
        data = requests.get(
            url=url, params={"q": artist, "access_token": genius_token}
        ).json()
        result = data["response"]["hits"]
        return result
    except Exception as e:
        print(str(e))
        return []


def get_song(id):
    try:
        url = "https://api.genius.com/songs/" + id
        data = requests.get(url=url, params={"access_token": genius_token}).json()
        return data["response"]

    except Exception as e:
        print(str(e))
        return {}


def get_top_tracks(id, bearer_token, market):
    headers = {"Authorization": "Bearer " + bearer_token}
    try:
        url = "http://api.spotify.com/v1/artists/" + id + "/top-tracks"
        data = requests.get(url=url, headers=headers, params={"market": market}).json()
        return data["tracks"]
    except Exception as e:
        print(str(e))
        return []


def get_albums(id, bearer_token):
    headers = {"Authorization": "Bearer " + bearer_token}
    try:
        url = "http://api.spotify.com/v1/artists/" + id + "/albums"
        data = requests.get(url=url, headers=headers)
        if "error" in data:
            print(data)
        return data.json()["items"]
    except Exception as e:
        print(str(e))
        return []


def get_related(id, bearer_token):
    headers = {"Authorization": "Bearer " + bearer_token}
    try:
        url = "http://api.spotify.com/v1/artists/" + id + "/related-artists"
        data = requests.get(url=url, headers=headers).json()
        return data["artists"]
    except Exception as e:
        print(str(e))
        return []


# Events


def create_event_thread_worker(
    id,
    artist_name,
    artist_image_url,
    facebook_page_url,
    event_url,
    sale_time,
    date,
    description,
    title,
    country,
    venue_name,
    latitude,
    longitude,
    city,
    lineup,
):
    # We're using the session context here.
    with session_scope() as session:
        # time.sleep(1)
        crud.create_event(
            session,
            id,
            artist_name,
            artist_image_url,
            facebook_page_url,
            event_url,
            sale_time,
            date,
            description,
            title,
            country,
            venue_name,
            latitude,
            longitude,
            city,
            lineup,
        )


def scrapeEventsURIS(bearer_token, db: Session):  # (bearer_token, db: Session)
    # Get Artist Names from csv(Database along with artist id's, provide event id's for artist database)
    artists = pd.read_csv("top-200-artists.csv")
    artists = pd.read_csv("top-200-artists.csv")
    artist_ids = artists["id"]
    artist_names = artists["name"]
    artist_pair = zip(artist_names.values, artist_ids.values)

    for pair in artist_pair:
        scrapeEventsByArtist(bearer_token, db, pair[0], pair[1])
        time.sleep(0.75)


# Returns 0 if we can't get events for the artist, 1 if we can and adds it
def scrapeEventsByArtist(
    bearer_token, db: Session, artist_name, artist_id, events_per_artist=6
):
    # Get Artist object from Bandsintown
    artist_url = "https://rest.bandsintown.com/artists/"
    event_url = "/events"
    auth = "?app_id=" + bearer_token
    event_ender = "&date=upcoming"

    # Get Artist object from Bandsintown
    url = artist_url + artist_name + auth
    artist_response = requests.get(url=url).json()
    reps = events_per_artist
    try:
        if artist_response is None:
            return 0
        # Get Events for Artist from Bandsintown
        elif artist_response["upcoming_event_count"] > 0:
            url = artist_url + artist_response["name"] + event_url + auth + event_ender
            event_response = requests.get(url=url).json()
            for event in event_response:
                if reps <= 0:
                    break
                else:
                    reps -= 1

                # Assemble City Model From CityName
                """
                if (not buildCityMethod()) #This would also build the mapping as well on success
                    return 0
                """
                # Assemble Events Model, get events model id
                threading.Thread(
                    target=create_event_thread_worker,
                    args=(
                        event["id"],
                        artist_response["name"],
                        artist_response["image_url"],
                        artist_response["facebook_page_url"],
                        event["url"],
                        event["on_sale_datetime"],
                        event["datetime"],
                        event["description"],
                        event["title"],
                        event["venue"]["country"],
                        event["venue"]["name"],
                        event["venue"]["latitude"],
                        event["venue"]["longitude"],
                        event["venue"]["city"],
                        event["lineup"],
                    ),
                ).start()

        else:
            return 0
    except Exception as e:
        return 0
