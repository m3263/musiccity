from os import name
from sqlalchemy.orm import Session
from sqlalchemy.sql import exists
import models
import requests
from contextlib import contextmanager
from connection import SessionLocal
import time
from models import Cities


@contextmanager
def session_scope():
    """Provide a transactional scope around a series of operations."""
    db = SessionLocal()
    try:
        yield db
        db.commit()
    except:
        db.rollback()
        raise
    finally:
        db.close()


def find_city_on_geodb(city_name):
    URL = "http://geodb-free-service.wirefreethought.com/v1/geo/cities"
    params = {
        "namePrefix": city_name,
        "types": "city",
        "sort": "-population",
        "limit": 10,
    }
    response = requests.get(url=URL, params=params)
    response_json = response.json()
    if len(response_json["data"]) == 0:
        return {}
    assert len(response_json["data"]) >= 1
    city = response_json["data"][0]
    city_id = city["id"]
    city_wiki_id = city["wikiDataId"]
    return {
        "geodb_id": city_id,
        "geodb_wiki_id": city_wiki_id,
    }


def get_city_details(city_wiki_id):
    URL = "http://geodb-free-service.wirefreethought.com/v1/geo/cities"
    response = requests.get(url=f"{URL}/{city_wiki_id}")
    response_json = response.json()
    assert response_json["data"]
    city_details = response_json["data"]
    return {
        "name": city_details["name"],
        "state": city_details["region"],
        "country": city_details["country"],
        "population": city_details["population"],
        "latitude": city_details["latitude"],
        "longitude": city_details["longitude"],
        "timezone": city_details["timezone"],
        "elevation": city_details["elevationMeters"],
    }


def get_city_weather(longitude, latitude):
    URL = "http://api.openweathermap.org/data/2.5/weather"
    API_KEY = "8e0b85b1b43db6f7742f8e1a78a56f52"
    weather_params = {
        "lat": latitude,
        "lon": longitude,
        "units": "imperial",
        "appid": API_KEY,
    }
    response = requests.get(url=URL, params=weather_params)
    weather = response.json()["main"]
    return {
        "temperature": weather["temp"],
        "humidity": weather["humidity"],
    }


def get_image_urls(city_name):
    URL = "https://contextualwebsearch-websearch-v1.p.rapidapi.com/api/Search/ImageSearchAPI"
    HEADERS = {
        "x-rapidapi-host": "contextualwebsearch-websearch-v1.p.rapidapi.com",
        "x-rapidapi-key": "644cf0fa81msh20577ab94a3fd45p196836jsn7fed5158ba30",
    }
    image_params = {
        "q": f"{city_name} skyline",
        "pageNumber": "1",
        "pageSize": "5",
        "autoCorrect": "true",
    }

    response = requests.get(url=URL, params=image_params, headers=HEADERS)
    images = response.json()
    return {"image1": images["value"][0]["url"], "image2": images["value"][1]["url"]}


def get_youtube_url(city_name):
    URL = "https://youtube-v31.p.rapidapi.com/search"
    HEADERS = {
        "x-rapidapi-host": "youtube-v31.p.rapidapi.com",
        "x-rapidapi-key": "644cf0fa81msh20577ab94a3fd45p196836jsn7fed5158ba30",
    }
    PARAMS = {
        "q": f"{city_name} vacation travel guide",
        "part": "snippet,id",
        "regionCode": "US",
        "maxResults": "5",
    }

    response = requests.get(url=URL, params=PARAMS, headers=HEADERS)
    response_json = response.json()
    youtube_id = response_json["items"][0]["id"]["videoId"]
    youtube_url = f"https://www.youtube.com/watch?v={youtube_id}"
    return {"youtube_url": youtube_url}


def scrape_city(city_name):
    result = {}
    result.update(find_city_on_geodb(city_name))
    time.sleep(0.5)
    result.update(get_city_details(result["geodb_wiki_id"]))
    result.update(get_city_weather(result["longitude"], result["latitude"]))
    result.update(get_image_urls(city_name))
    result.update(get_youtube_url(city_name))
    return result


def city_already_exists_in_db(db, city_name):
    geodb_wiki_id = db.query(Cities.id).filter_by(name=city_name).first()
    if geodb_wiki_id != None:
        return True
    else:
        return False


def create_city(db, city_name):
    if city_already_exists_in_db(db, city_name):
        return
    city_json = scrape_city(city_name)
    db_city = models.Cities(
        id=city_json["geodb_wiki_id"],
        name=city_json["name"],
        state=city_json["state"],
        country=city_json["country"],
        elevation=city_json["elevation"],
        humidity=city_json["humidity"],
        temperature=city_json["temperature"],
        population=city_json["population"],
        geodb_id=city_json["geodb_id"],
        geodb_wiki_id=city_json["geodb_wiki_id"],
        image1=city_json["image1"],
        image2=city_json["image2"],
        youtube_url=city_json["youtube_url"],
        latitude=city_json["latitude"],
        longitude=city_json["longitude"],
        timezone=city_json["timezone"],
    )
    db.merge(db_city)
    db.commit()


def add_city_to_db(session, city_name):
    create_city(session, city_name)


def addSuccessfulMapping(artistId, eventId, city_geodb_wiki_id):
    f = open("successfulMappings.txt", "a")
    f.write(f"{artistId},{eventId},{city_geodb_wiki_id}\n")
    f.close()


def addUnsuccessfulMapping(artistId, eventId, cityName):
    f = open("unsuccessfulMappings.txt", "a")
    f.write(f"{artistId},{eventId},{cityName}\n")
    f.close()


def read_mappings_file(session):
    f = open("mappings.txt", "r")
    l = f.readlines()
    mappings = []
    citiesAddedAlready = {}
    for line in l:
        mapping = line[1:-3].split(",")
        for i in range(3):
            mapping[i] = mapping[i].strip()
        mappings.append(mapping)
    for artistId, eventId, cityName in mappings:
        city_geodb_wiki_id = None
        if cityName in citiesAddedAlready:
            city_geodb_wiki_id = citiesAddedAlready[cityName]
            addSuccessfulMapping(artistId, eventId, city_geodb_wiki_id)
        else:
            result = find_city_on_geodb(cityName)
            time.sleep(0.9)
            if len(result) == 0:
                addUnsuccessfulMapping(artistId, eventId, cityName)
                continue
            else:
                city_geodb_wiki_id = result["geodb_wiki_id"]
                add_city_to_db(session, cityName)
                citiesAddedAlready[cityName] = city_geodb_wiki_id
                addSuccessfulMapping(artistId, eventId, city_geodb_wiki_id)


with session_scope() as session:
    read_mappings_file(session)
