from sqlalchemy.orm import Session
import models
import schemas
import crud
import pandas as pd
import requests
import threading
import scrape
from contextlib import contextmanager
from connection import SessionLocal
import time


def scrapeEventsURIS():  # (bearer_token, db: Session)
    # Get Artist Names from csv(Database along with artist id's, provide event id's for artist database)
    artists = pd.read_csv("top-200-artists.csv")
    artist_ids = artists["id"]
    artist_names = artists["name"]
    artist_pair = zip(artist_names.values, artist_ids.values)

    artist_url = "https://rest.bandsintown.com/artists/"
    event_url = "/events"
    auth = "?app_id=1a43de5dfa5dbedf7affd15013a6c393"
    event_ender = "&date=upcoming"

    # url = artist_url + arty[0] + auth
    # artist_response = requests.get(url=url).json()
    token = "1a43de5dfa5dbedf7affd15013a6c393"
    no_show = {}
    error_log = ""
    cities_involved = set()
    events_involved = set()
    events_per_artist = 6
    for pair in artist_pair:
        scrapeEventsForArtist(
            token,
            pair[0],
            pair[1],
            no_show,
            cities_involved,
            events_involved,
            events_per_artist,
            error_log,
        )
        time.sleep(0.75)
    print("Artists w/out shows : " + str(len(no_show)))
    print("Unique Cities : " + str(len(cities_involved)))
    print("Unique Events : " + str(len(events_involved)))
    print(no_show)
    print(error_log)
    # list of artist


def scrapeEventsForArtist(
    bearer_token,
    artist_name,
    artist_id,
    no_show,
    cities_involved,
    events_involved,
    events_per_artist,
    error_log,
):
    # Get Artist object from Bandsintown
    artist_url = "https://rest.bandsintown.com/artists/"
    event_url = "/events"
    auth = "?app_id=" + bearer_token
    event_ender = "&date=upcoming"

    url = artist_url + artist_name + auth
    artist_response = requests.get(url=url).json()
    try:
        if artist_response is None:
            print("NO, RESPONSE")
        elif artist_response["upcoming_event_count"] > 0:
            url = artist_url + artist_response["name"] + event_url + auth + event_ender
            event_response = requests.get(url=url).json()
            reps = events_per_artist
            for event in event_response:
                if reps <= 0:
                    break
                else:
                    reps -= 1
                cities_involved.add(event["venue"]["city"])
                events_involved.add(event["id"])
                #                                                               id,     artist_name,                artist_image_url,           facebook_page_url,                      event_url,  sale_time,                      date,           description,         title,         country,             venue_name,            latitude, l                 ongitude,                   city,                   lineup
                threading.Thread(
                    target=scrape.create_event_thread_worker,
                    args=(
                        event["id"],
                        artist_response["name"],
                        artist_response["image_url"],
                        artist_response["facebook_page_url"],
                        event["url"],
                        event["on_sale_datetime"],
                        event["datetime"],
                        event["description"],
                        event["title"],
                        event["venue"]["country"],
                        event["venue"]["name"],
                        event["venue"]["latitude"],
                        event["venue"]["longitude"],
                        event["venue"]["city"],
                        event["lineup"],
                    ),
                ).start()
                print(
                    "("
                    + artist_id
                    + ", "
                    + event["id"]
                    + ", "
                    + event["venue"]["city"]
                    + "),"
                )
        else:
            no_show[artist_name] = artist_id
    except Exception as e:
        no_show[artist_name] = artist_id
        error_log = (
            "When handling artist : " + artist_name + "| Error : " + repr(e) + "\n"
        )


def test0():
    print(str(1))


if __name__ == "__main__":
    scrapeEventsURIS()
