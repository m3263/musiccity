#!/bin/sh

ssh -o StrictHostKeyChecking=no ec2-user@54.197.122.57 << 'ENDSSH'
  cd /home/ec2-user
  export $(cat .env | xargs)
  $(aws ecr get-login --no-include-email --region us-east-1)
  docker pull 165181753670.dkr.ecr.us-east-1.amazonaws.com/musiccity-backend:latest
  docker kill $(docker ps -q)
  docker run --rm -d -p 80:80 -e spotify_client_id=$spotify_client_id -e spotify_client_secret=$spotify_client_secret -e genius_token=$genius_token -e SQLALCHEMY_DATABASE_URL=$SQLALCHEMY_DATABASE_URL 165181753670.dkr.ecr.us-east-1.amazonaws.com/musiccity-backend:latest
ENDSSH
