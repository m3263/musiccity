# MusicCity

Members:
Name: Thomas Connor Thompson
UTEID: tct572
GitLab: thocotho

Name: Anna Thomas
UTEID: aet2384
GitLab: annathomas2384

Name: Anirudh Goyal
UTEID: ag69397
GitLab: anirudhgoyal

Name: Noah Kim
UTEID: yk7679
GitLab: yk0610

Name: Anshul Modh
UTEID: amm9522
GitLab: anshulmodh

GitSHA 1: 86cae67072c788e850ca6c782e5404a452940408
GitSHA 2: a8fc7c121cbf2e58f0300d4517bbe9fec4139477
GitSHA 3: c17bca29e3241230d95bc2b80c715f2b7d5e1155
GitSHA 4: 1f9475f8a4fc994cbeeea44e8ea52fac450402ce

Project Leader
	Phase 1: Anshul Modh
	Phase 2: Anirudh Goyal
	Phase 3: Connor Thomson
	Phase 4: Noah Kim

GitLab Pipeline: https://gitlab.com/m3263/musiccity/-/pipelines

Project Website: https://musiccity.website

Phase 1: 
Estimated Completion Time: Thomas: 13 Anna: 14 Anirudh: 11 Noah: 15 Anshul: 15

Actual Completion Time: Thomas: 15 Anna: 15 Anirudh: 15 Noah: 15 Anshul: 15

Phase 2: 
Estimated Completion Time: Thomas: 21 Anna: 17 Anirudh: 18 Noah: 17 Anshul: 20

Actual Completion Time: Thomas: 18 Anna: 18 Anirudh: 20 Noah: 19 Anshul: 45

Phase 3: 
Estimated Completion Time: Thomas: 13 Anna: 11 Anirudh: 12 Noah: 16 Anshul: 15

Actual Completion Time: Thomas: 14 Anna: 13 Anirudh: 14 Noah: 18 Anshul: 20

Phase 4:
Estimated Completion Time: Thomas: 11 Anna: 8 Anirudh: 9 Noah: 8 Anshul: 12

Actual Completion Time: Thomas: 10  Anna: 10 Anirudh: 10 Noah: 9 Anshul: 12


Comments:

Citations:

https://gitlab.com/forbesye/fitsbits/

https://gitlab.com/bevos-crew/bevos-course-guide

https://recharts.org/en-US/

http://bl.ocks.org/wiesson/ef18dba71256d526eb42

https://www.d3-graph-gallery.com/graph/bubblemap_template.html


