.DEFAULT_GOAL := all
MAKEFLAGS += --no-builtin-rules
SHELL         := bash
BLACK         := black

format:
	$(BLACK) back-end/app/add_cities.py
	$(BLACK) back-end/app/add_events_from_list.py
	$(BLACK) back-end/app/connection.py
	$(BLACK) back-end/app/crud.py
	$(BLACK) back-end/app/main.py
	$(BLACK) back-end/app/models.py
	$(BLACK) back-end/app/schemas.py
	$(BLACK) back-end/app/scrape.py
	$(BLACK) back-end/app/unittests.py
	
docker-run-backend:
	cd back-end
	docker run --rm -t -i -p 8000:80 musiccity-backend

docker-run-backend-bg:
	docker run --rm -d -p 8000:80 musiccity-backend

docker-dev:
	docker run -it -p 8000:8000 -v $(PWD)/back-end/app:/app/ mc-backend-dev

test-backend:
	pip install -r back-end/requirements.txt
	cd back-end/app
	cd back-end/app && python3 -m pytest unittests.py -vv

test-selenium:
	cd front-end/guitests && pytest guitests.py

all:

install-frontend:
	cd front-end
	npm install
	cd ..

# check files, check their existence with make check
CFILES :=                                 \
    .gitignore                            \
    .gitlab-ci.yml                        

check: $(CFILES)
# remove temporary files
clean:
	rm -f  *.tmp

test-frontend:
	cd front-end/
	npm install
	npm test -- --watchAll=false

test-postman:
	npm install
	newman --version
	newman run postman.json

docker-start:
	docker pull thocotho/webdev
	docker run -it -p 3000:3000 -p 35729:35729 -v $(PWD):/usr/typescript -w /usr/typescript thocotho/webdev
	
run-front:
	cd ./front-end; npm install; CHOKIDAR_USEPOLLING=true npm start; 
	
docker-run-front:
	docker pull thocotho/webdev;
	docker run --rm --name front_end_server -p 3000:3000 -p 35729:35729 -v $(PWD):/usr/typescript -w /usr/typescript thocotho/webdev make run-front

docker-run-front-bg:
	docker pull thocotho/webdev;
	docker run --rm --name front_end_server -d -p 3000:3000 -p 35729:35729 -v $(PWD):/usr/typescript -w /usr/typescript thocotho/webdev make run-front