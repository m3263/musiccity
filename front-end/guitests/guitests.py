from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver import Remote
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

import os
import time

base_url = "https://stage.musiccity.website"

class TestSelenium:

    # Inspired by bevoscourseguide.me here: https://gitlab.com/bevos-crew/bevos-course-guide/-/blob/SeleniumUnitTests/frontend/guitests/gui_test.py
    def setup_class(self):
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        files = os.listdir()
        if "chromedriver" in files:
            s = Service('./chromedriver')
            driver = webdriver.Chrome(service=s, options=chrome_options)
        elif "mac_chromedriver" in files:
            s = Service('./mac_chromedriver')
            driver = webdriver.Chrome(service=s, options=chrome_options)
        elif "chromedriver.exe" in files:
            s = Service('./chromedriver.exe')
            driver = webdriver.Chrome(service=s, options=chrome_options)
        else:
            driver = Remote("http://selenium__standalone-chrome:4444/wd/hub", desired_capabilities=chrome_options.to_capabilities())
        driver.get(base_url)
        wait = WebDriverWait(driver, 20)
        self.driver = driver

    # Test 1
    def test_title(self):
        self.driver.get(base_url)
        assert self.driver.title == "MusicCity"
    # Test 2
    def test_navbar_home(self):
        self.driver.get(base_url)
        navbar = self.driver.find_element_by_xpath("/html/body/div/div/div[1]/header/div/a[1]")
        assert navbar
    # Test 3
    def test_navbar_to_about(self):
        self.driver.get(base_url)
        navbar_about_button = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH, "/html/body/div/div/div[1]/header/div/a[2]/button")))
        navbar_about_button.click()
        about_title = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH, "/html/body/div/div/div[2]/div/div[1]/div")))
        assert about_title
    # Test 4
    def test_navbar_to_artists(self):
        self.driver.get(base_url)
        navbar_artists_button = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[1]/header/div/a[3]/button"))) 
        navbar_artists_button.click()
        ed_card = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[2]/div/div[2]/div[1]/div/div/div/div[1]/a")))
        assert ed_card
    # Test 5
    def test_navbar_to_cities(self):
        self.driver.get(base_url)
        navbar_cities_button = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[1]/header/div/a[4]/button")))
        navbar_cities_button.click()
        table = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[2]/div/div[2]/div/table")))
        assert table
    # Test 6
    def test_navbar_to_events(self):
        self.driver.get(base_url)
        navbar_events_button = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[1]/header/div/a[5]/button")))
        navbar_events_button.click()
        table = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[2]/div/div[2]/div/div/table/thead")))
        assert table
    # Test 7
    def test_navbar_to_home(self):
        self.driver.get(base_url)
        navbar_events_button = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[1]/header/div/a[4]/button"))) 
        navbar_events_button.click()
        homebutton = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[1]/header/div/a[1]/button")))
        homebutton.click()
        titleText = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[2]/div[1]/div/div/div")))
        assert titleText.text == "Welcome to Music City!"
    # Test 8
    def test_artist_instance(self):
        self.driver.get(base_url)
        navbar_artists_button = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[1]/header/div/a[3]/button"))) 
        navbar_artists_button.click()
        ed_card = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[2]/div/div[2]/div[1]/div/div/div/div[1]/a")))
        ed_card.click()
        element = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[2]/div/div[1]/div/div/div/div[1]")))
        assert element
    # Test 9
    def test_city_instance(self):
        self.driver.get(base_url)
        navbar_cities_button = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[1]/header/div/a[4]/button"))) 
        navbar_cities_button.click()
        row = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[2]/div/div[2]/div/table/tbody/a[1]")))
        row.click()
        element = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[2]/div/div[1]/div")))
        assert element
    # Test 10
    def test_event_instance(self):
        self.driver.get(base_url)
        navbar_events_button = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[1]/header/div/a[5]/button"))) 
        navbar_events_button.click()
        row = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[2]/div/div[2]/div/div/table/tbody/a[1]")))
        row.click()
        element = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[2]/div/div[2]/div/div[5]/img")))
        assert element

   # Test 11
    def test_sorting_artists(self):
        self.driver.get(base_url)
        navbar_artists_button = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[1]/header/div/a[3]/button"))) 
        navbar_artists_button.click()
        sort_by = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[2]/div/div[1]/div/div/form/div[4]/div/div"))) 
        sort_by.click()
        sort_by_dropdown = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div[2]/div[3]/ul/li[2]"))) 
        sort_by_dropdown.click()
        ed_card = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div[1]/div/div[2]/div/div[2]/div[1]/div/div/div/div[1]/a")))
        assert ed_card
        
        sort_order = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[2]/div/div[1]/div/div/form/div[5]/div/div"))) 
        sort_order.click()
        sort_order_dropdown = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div[2]/div[3]/ul/li[2]")))
        sort_order_dropdown.click()
        ed_card = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[2]/div/div[2]/div[1]/div/div/div/div[1]/a")))
        ed_card.click()
        assert "https://stage.musiccity.website/artists/4q3ewBCX7sLwd24euuV69X" in self.driver.current_url
    # Test 12
    def test_filtering_artists(self):
        self.driver.get(base_url)
        navbar_artists_button = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[1]/header/div/a[3]/button"))) 
        navbar_artists_button.click()
        genre_filter = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[2]/div/div[1]/div/div/form/div[2]/div/div")))
        genre_filter.click()
        genre_filter_by = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div[2]/div[3]/ul/li[5]")))
        genre_filter_by.click()
        WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div[2]/div[1]"))).click()
        ed_card = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[2]/div/div[2]/div[1]/div/div/div/div[1]/a")))
        assert ed_card

        related_filter = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[2]/div/div[1]/div/div/form/div[3]/div/div")))
        related_filter.click()
        related_filter_by = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div[2]/div[3]/ul/li[1]")))
        related_filter_by.click()
        ed_card = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[2]/div/div[2]/div[1]/div/div/div/div[1]/a")))
        assert ed_card
    # Test 13
    def test_searching_artists(self):
        self.driver.get(base_url)
        navbar_artists_button = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[1]/header/div/a[3]/button"))) 
        navbar_artists_button.click()
        search_input = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[2]/div/div[1]/div/div/form/div[1]/input")))
        search_input.send_keys("sheeran")
        search_button = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[2]/div/div[1]/div/div/form/button")))
        search_button.click()
        ed_card = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[2]/div/div[2]/div[1]/div/div/div/div[1]/a")))
        ed_card.click()
        assert "https://stage.musiccity.website/artists/6eUKZXaKkcviH0Ku9w2n3V" in self.driver.current_url
    # Test 14
    def test_sorting_cities(self):
        self.driver.get(base_url)
        navbar_cities_button = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[1]/header/div/a[4]/button"))) 
        navbar_cities_button.click()
        sort_by = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[2]/div/div[1]/div/div/form/div[4]/div/div"))) 
        sort_by.click()
        sort_by_dropdown = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div[2]/div[3]/ul/li[3]"))) 
        sort_by_dropdown.click()
        
        sort_order = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[2]/div/div[1]/div/div/form/div[5]/div/div"))) 
        sort_order.click()
        sort_order_dropdown = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div[2]/div[3]/ul/li[2]")))
        sort_order_dropdown.click()
        row = WebDriverWait(self.driver,10).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[2]/div/div[2]/div/table/tbody/a[1]")))
        row.click()
        assert "https://stage.musiccity.website/cities/Q16556" in self.driver.current_url
    # Test 15
    def test_filtering_cities(self):
        self.driver.get(base_url)
        navbar_cities_button = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[1]/header/div/a[4]/button"))) 
        navbar_cities_button.click()
        country_filter = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[2]/div/div[1]/div/div/form/div[2]/div/div")))
        country_filter.click()
        country_filter_by = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div[2]/div[3]/ul/li[4]")))
        country_filter_by.click()
        WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div[2]/div[1]"))).click()

        state_filter = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[2]/div/div[1]/div/div/form/div[3]/div/div")))
        state_filter.click()
        state_filter_by = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div[2]/div[3]/ul/li[1]")))
        state_filter_by.click()
        row = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[2]/div/div[2]/div/table/tbody/a[1]")))
        assert row
    # Test 16
    def test_searching_cities(self):
        self.driver.get(base_url)
        navbar_cities_button = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[1]/header/div/a[4]/button"))) 
        navbar_cities_button.click()
        search_input = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[2]/div/div[1]/div/div/form/div[1]/input")))
        # search_input.send_keys("texas")
        search_button = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[2]/div/div[1]/div/div/form/button")))
        # search_button.click()
        # row = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[2]/div/div[2]/div/table/tbody/a[1]")))
        # row.click()
        # assert "https://stage.musiccity.website/cities/Q17943" in self.driver.current_url
        assert search_input
        assert search_button
    # Test 17
    def test_sorting_events(self):
        self.driver.get(base_url)
        navbar_events_button = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[1]/header/div/a[5]/button")))
        navbar_events_button.click()
        sort_by = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[2]/div/div[1]/div/div/form/div[2]/div/div")))
        sort_by.click()
        sort_by_dropdown = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div[2]/div[3]/ul/li[3]")))
        sort_by_dropdown.click()

        sort_order = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[2]/div/div[1]/div/div/form/div[3]/div/div"))) 
        sort_order.click()
        sort_order_dropdown = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div[2]/div[3]/ul/li[1]")))
        sort_order_dropdown.click()
        row = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[2]/div/div[2]/div/div/table/tbody/a[1]")))
        row.click()
        assert "https://stage.musiccity.website/events/1021449402" in self.driver.current_url
    # Test 18
    def test_searching_events(self):
        self.driver.get(base_url)
        navbar_events_button = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[1]/header/div/a[5]/button")))
        navbar_events_button.click()
        search_input = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[2]/div/div[1]/div/div/form/div[1]/input")))
        search_input.send_keys("texas")
        search_input.send_keys(Keys.ENTER)
        # search_button = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[2]/div/div[1]/div/div/form/button")))
        # search_button.click()
        row = WebDriverWait(self.driver,10).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[2]/div/div[2]/div/div/table/tbody/a[1]")))
        row.click()
        assert "https://stage.musiccity.website/events/1023085792" in self.driver.current_url
    # Test 19
    def test_searching_sitewide(self):
        self.driver.get(base_url)
        search_input = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[2]/div[2]/div[2]/form/div/input")))
        search_input.send_keys("texas")
        search_button = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[2]/div[2]/div[2]/form/button")))
        search_button.click()
        artist_button = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[2]/div[2]/div[2]/div/div/div/button")))
        events_button = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[2]/div[4]/div/button")))
        cities_button = WebDriverWait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"/html/body/div/div/div[2]/div[6]/div/button")))
        assert artist_button
        assert events_button
        assert cities_button