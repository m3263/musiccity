import React, { useState, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { Grid, Typography, Card, CardMedia, CardContent, CardActionArea, Paper, InputBase, Box, FormControlLabel, Switch, CircularProgress } from '@mui/material/';
import IconButton from '@mui/material/IconButton';
import SearchIcon from '@mui/icons-material/Search';
import { getAllData } from './functions/fetchAllData'
import ArtistsView from './Artists/ArtistsView'
import CitiesTable from './Cities/CitiesTable'
import EventsTable from './Events/EventsTable'

function Search({location}) {
    let params = new URLSearchParams(location.search);
    const history = useHistory();
    const [artists, setArtists] = useState([])
    const [citiesCount, setCitiesCount] = useState(0)
    const [eventsCount, setEventsCount] = useState(0)
    const [events, setEvents] = useState([])
    const [cities, setCities] = useState([])
    const [search, setSearch] = useState(params.get("query"))
    const [fahren, setFahren] = useState(true)

    useEffect(() => {
        getAllData(search)
        .then(response => {
            let items = response['artists']['results'];
            items.push({
                "name": "View More Artists Here",
                "count": response['artists']['count'],
                "path": "/artists?query=" + search + "&sort=name&order=asc"
            })
            setArtists(items);
            setCitiesCount(response['cities']['count'])
            setEventsCount(response['events']['count'])
            setEvents(response['events']['results']);
            setCities(response['cities']['results']);
        })
    }, [])
    
    const validate = (event: any) => {
        if (event.key === 'Enter'){
            event.preventDefault() 
            test(event)
        }
    }

    const test = (event: any) => {
        setArtists([]);
        setCitiesCount(0)
        setEventsCount(0)
        setEvents([]);
        setCities([]);
        getAllData(search)
        .then(response => {
            let items = response['artists']['results'].slice(0, 9);
            items.push({
                "name": "View More Artists Here",
                "count": response['artists']['count'],
                "path": "/artists?query=" + search + "&sort=name&order=asc"
            })
            setArtists(items);
            setCitiesCount(response['cities']['count'])
            setEventsCount(response['events']['count'])
            setEvents(response['events']['results']);
            setCities(response['cities']['results']);
        })
        history.push("search?query=" + search);
    }

    const handleSearch = (event: any) => {
        setSearch(event.target.value);
    };

    const handleTemp = (event: React.ChangeEvent<HTMLInputElement>) => {
        setFahren(event.target.checked);
      };

    return (
        <Grid container rowSpacing={5} columnSpacing={5} alignItems="center" justifyContent="center" p={10}>
            <Grid item xs={12}>
                <Typography gutterBottom variant="h4" component="div">
                    Search All
                </Typography>
                <Grid container justifyContent="center">
                    <Paper
                        component="form"
                        sx={{ p: '2px 4px', display: 'flex', alignItems: 'center', width: 800 }}
                    >
                        <InputBase
                            sx={{ ml: 1, flex: 1 }}
                            placeholder="Search"
                            inputProps={{ 'aria-label': 'search' }}
                            value={search}
                            onChange={handleSearch}
                            onKeyDown={validate}
                        />
                        <IconButton onClick={test} sx={{ p: '10px' }} aria-label="search">
                            <SearchIcon />
                        </IconButton>
                    </Paper>
                </Grid>
            </Grid>
            <Grid item xs={12}>
                <Typography gutterBottom variant="h4" component="div" mt={5}>
                    Artists
                </Typography>
                {artists.length === 0 ? (
                    <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center'}} mt={10}>
                        <CircularProgress />
                    </Box>
                ) : (
                    <ArtistsView artists={artists} search={search} genre={[]} related={[]}/>
                )}
            </Grid>
            <Grid item xs={12}>
                <Typography gutterBottom variant="h4" component="div" mt={5}>
                    Events
                </Typography>
                {events.length === 0 ? (
                    <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center'}} mt={10}>
                        <CircularProgress />
                    </Box>
                ) : (
                    <EventsTable events={events} search={search}/>
                )}
            </Grid>
            {events.length !== 0 ? (
                <Grid container alignItems="center" justifyContent="center" sx={{ maxWidth: 220 }} mt={5}>
                    <Grid item xs={12}>
                        <CardActionArea>
                            <Link to={"/events?query=" + search} style={{textDecoration: 'none'}}>
                                <Card>
                                    <Box>
                                        <Typography variant="h6">
                                            Check Out More Events
                                        </Typography>
                                    </Box>
                                    <Box>
                                        <Typography variant="h6">
                                            {eventsCount} Results
                                        </Typography>
                                    </Box>
                                </Card>
                            </Link>
                        </CardActionArea>
                    </Grid>
                </Grid>
            ) : (null)}
            <Grid item xs={12}>
                <Typography gutterBottom variant="h4" component="div" mt={5}>
                    Cities
                </Typography>
                <FormControlLabel control={<Switch checked={fahren} onChange={handleTemp} defaultChecked />} label={fahren ? "°F" : "°C"} />
                {cities.length === 0 ? (
                    <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center'}} mt={10}>
                        <CircularProgress />
                    </Box>
                ) : (
                    <CitiesTable cities={cities} search={search} fahren={fahren}/>
                )}
            </Grid>
            {cities.length !== 0 ? (
                <Grid container alignItems="center" justifyContent="center" sx={{ maxWidth: 220 }} mt={5}>
                <Grid item xs={12}>
                    <CardActionArea>
                        <Link to={"/cities?query=" + search} style={{textDecoration: 'none'}}>
                            <Card>
                                <Box>
                                    <Typography variant="h6">
                                        Check Out More Cities
                                    </Typography>
                                </Box>
                                <Box>
                                    <Typography variant="h6">
                                        {citiesCount} Results
                                    </Typography>
                                </Box>
                            </Card>
                        </Link>
                    </CardActionArea>
                </Grid>
            </Grid>
            ) : (null)}
        </Grid>
    );
}

export default Search