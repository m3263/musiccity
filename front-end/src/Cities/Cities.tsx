import React, { useState, useEffect } from 'react';
import { Grid, Typography, Box, CircularProgress } from '@mui/material/';
import CustomPagination from '../shared_components/CustomPagination'
import CitiesTable from './CitiesTable'
import CitySelection from './CitySelection'
import { getCities } from '../functions/fetchCityData'


function Cities({location}) {
    let params = new URLSearchParams(location.search);
    const [count, setCount] = useState(10); 
    const [loading, setLoading] = useState<boolean>(true);
    const [cities, setCities]: [any, any] =  useState([]);
    const [sort, setSort] = useState(params.get("sort") === null ? 'city' : params.get("sort")); 
    const [sortOrder, setSortOrder] = useState(params.get("order") === null ? "asc" : params.get("order")); 
    const [page, setPage] = useState(params.get("page") === null ? 0 : parseInt(params.get("page"))); 
    const [itemsPerPage, setItemsPerPage] = useState(params.get("step") === null ? 10 : parseInt(params.get("step"))); 
    const [search, setSearch] = useState(params.get("query") === null ? "" : params.get("query"));
    const [country, setCountry] = React.useState(params.get("country") === null || params.get("country") === "" ? [] : params.get("country").split(','));
    const [state, setState] = React.useState(params.get("state") === null || params.get("state") === "" ? [] : params.get("state").split(','));
    const [fahren, setFahren] = React.useState(true);
    

    useEffect(() => {
        getData();
    }, [page, itemsPerPage, country, state, sort, sortOrder]);

    const getData = async() => {
        const cityData = await getCities(page, itemsPerPage, search, state.join(","), country.join(","), sort, sortOrder);      
        setCount(cityData['count']);
        setCities(cityData['results']);
        setLoading(false);
    }

    return (
            <Grid container alignItems="center" justifyContent="center"> 
                <Grid container rowSpacing={5} alignItems="center" justifyContent="center" sx={{ maxWidth: "80%" }}>
                    <Grid item xs={12}>
                        <CitySelection 
                            search={search} 
                            setSearch={setSearch} 
                            setCities={setCities} 
                            sortOrder={sortOrder} 
                            setSortOrder={setSortOrder} 
                            itemsPerPage={itemsPerPage} 
                            page={page} 
                            setCount={setCount} 
                            setPage={setPage} 
                            sort={sort} 
                            setSort={setSort}
                            state={state}
                            setState={setState}
                            country={country}
                            setCountry={setCountry}
                            fahren={fahren}
                            setFahren={setFahren}
                        />
                    </Grid>
                    {loading ? (
                        <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center'}} mt={10}>
                            <CircularProgress />
                        </Box>
                    ) : (
                        <Grid item xs={12}>
                            <CitiesTable fahren={fahren} cities={cities} search={search}/>
                        </Grid>
                    )}
                    <Grid item xs={12}>
                        <Grid container alignItems="center" justifyContent="center">
                            <CustomPagination page={page} setPage={setPage} itemsPerPage={itemsPerPage} setItemsPerPage={setItemsPerPage} count={count}/>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        );
}

export default Cities;