/* tslint:disable */
import React, { useEffect, useState } from 'react';
import { useParams, Link } from 'react-router-dom'
import { Grid, Typography, Box, Container, Card, CardContent, Paper, Table, TableContainer, TableBody, TableRow, TableCell, CircularProgress } from '@mui/material/';
import ReactPlayer from 'react-player'
import { getCityData } from '../functions/fetchCityData'

function CustomTableRow(props) {
    return (
        <TableRow hover role="checkbox" tabIndex={-1}>
        <TableCell>
            <b>{props.attribute}</b>
        </TableCell>
        <TableCell>
            {props.value}
        </TableCell>
        </TableRow>
    )
}

function SingleEventRow(props) {
    return (
        <TableRow hover role="checkbox" tabIndex={-1}>
        <TableCell>
            <b>{props.attribute}</b>
        </TableCell>
        <TableCell>
        <Link to={props.url}>
            {props.value}
        </Link>
        </TableCell>
        </TableRow>
    )
}

function getEventLink(id) {
    return "../events/" + id;
}

function getArtistLink(id) {
    return "../artists/" + id;
}

function City() {
    const params: any = useParams();
    const cityId = params.id;
    const [loading, setLoading] = useState<boolean>(true);
    const [data, setData]: [any, any] =  useState([]);
    
    useEffect(() => {
        getData();
    }, []);

    const getData = async() => {
        const cityData = await getCityData(cityId);        
        setData(cityData.data);
        setLoading(false);
    }

    if (!loading) {
        return (
            <div style={{ height: 1000, width: '100%' }}>
            <Container>
            <Grid container alignItems="center" justifyContent="center" mt={5}>
                <Grid container alignItems="center" justifyContent="center">
                    <Grid item xs={12}>
                        <Grid container justifyContent="center"> 
                            <Typography variant="h3">
                                {data.name}
                            </Typography>
                        </Grid>
                    </Grid>
                    <Grid item xs={12} mt={5}>
                        <Grid container justifyContent="center">    
                            <img src={data.image1}  width="300" height="300" alt={data.name} /> 
                            <img src={data.image2}  width="300" height="300" alt={data.name} /> 
                        </Grid>
                    </Grid>
                    <Grid item xs={12} mt={5}>
                        <Grid container justifyContent="center">
                            <ReactPlayer url={data.youtube_url}/>
                        </Grid>
                    </Grid>
                    <Grid item xs={12} mt={5}>
                        <Card sx={{ minWidth: 275 }}>
                            <CardContent>
                                    <Typography variant="h4" component="div" mb={5}>
                                        City Information
                                    </Typography>
                                    <Grid container justifyContent="center"> 
                                        <Paper sx={{ width: '90%', overflow: 'hidden' }}>
                                            <TableContainer>
                                                <Table stickyHeader aria-label="sticky table">
                                                    <TableBody>
                                                        <CustomTableRow attribute="Name" value={data.name}/>
                                                        <CustomTableRow attribute="State" value={data.state}/>
                                                        <CustomTableRow attribute="Country" value={data.country}/>
                                                        <CustomTableRow attribute="Latitude" value={data.latitude}/>
                                                        <CustomTableRow attribute="Longitude" value={data.longitude}/>
                                                        <CustomTableRow attribute="Population" value={data.population}/>
                                                        <CustomTableRow attribute="Timezone" value={data.timezone}/>
                                                        <CustomTableRow attribute="Temperature" value={data.temperature}/>
                                                        <CustomTableRow attribute="Elevation" value={data.elevation}/>
                                                        <CustomTableRow attribute="Humidity" value={data.humidity}/>
                                                    </TableBody>
                                                </Table>
                                            </TableContainer>
                                        </Paper>
                                    </Grid>
                            </CardContent>
                        </Card>
                    </Grid>
                    <Grid item xs={12} mt={5}>
                        <Card sx={{ minWidth: 275 }}>
                            <Container>
                                <Typography variant="h4" mb={5}>
                                    Upcoming Events In {data.name}
                                </Typography>
                                {data.links.slice(0, 3).map((link, index) => (
                                <Card sx={{ p: 0, m: 3 }}>
                                <TableContainer>
                                    <Table stickyHeader aria-label="sticky table">
                                        <TableBody>
                                            <TableRow hover role="checkbox" tabIndex={-1} key={1} component={Link} to={getArtistLink(link.artist.id)} style={{ textDecoration: 'none' }}>
                                                <TableCell>
                                                Artist
                                                </TableCell>
                                                <TableCell>
                                                    {link.artist.name}
                                                </TableCell>
                                            </TableRow>
                                            <TableRow hover role="checkbox" tabIndex={-1} key={1} component={Link} to={getEventLink(link.event.id)} style={{ textDecoration: 'none' }}>
                                                <TableCell>
                                                Event Venue Name
                                                </TableCell>
                                                <TableCell>
                                                    {link.event.venue_name}
                                                </TableCell>
                                            </TableRow>
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                                </Card>
                                    )
                                    )}

                            </Container>
                        </Card>
                    </Grid>
                </Grid>
            </Grid>
            </Container>
          </div>            
        )
    } else {
        return (
            <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center'}} mt={20}>
                <CircularProgress />
            </Box>
        );
    }
}

export default City;
