import React, { useEffect, useState } from 'react';
import { InputLabel, MenuItem, FormControl, Select, InputBase, Grid, Switch, ListItemText, Checkbox, FormControlLabel } from '@mui/material/';
import Paper from '@mui/material/Paper';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import SearchIcon from '@mui/icons-material/Search';
import { getCities } from '../functions/fetchCityData'
import { useHistory } from "react-router-dom";

const ITEM_HEIGHT = 100;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 270,
    },
  },
};

const CountryMenuProps = {
    PaperProps: {
      style: {
        maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
        width: 250,
      },
    },
  };

const states = ["Alberta","Andalusia","Arizona","Arkansas","Auckland Region","Baden-Württemberg","Berlin","Braga District","British Columbia","Budapest","California","Canterbury Region","Catalonia","Colorado","Community of Madrid","Emilia-Romagna","England","Florida","Gelderland","Georgia","Groningen","Hamburg","Île-de-France","Illinois","Kentucky","Leinster","Lima","Lisbon District","Louisiana","Lower Austria","Lower Saxony","Manitoba","Massachusetts","México","Mexico City","Michigan","Minnesota","Moscow","Munster","Nevada","New Jersey","New York","North Carolina","Northern Ireland","North Rhine-Westphalia","Nuevo León","Ohio","Ontario","Oslo","Pennsylvania","Pirkanmaa","Puebla","Quebec","Rio Grande do Sul","Saint Petersburg","Santiago del Estero","São Paulo","Scotland","South Carolina","South Dakota","South Holland","Stockholm County","Tamaulipas","Taranaki Region","Tennessee","Texas","Utah","Vestland","Vilnius County","Virginia","Wales","Washington","Wellington Region","Wisconsin"]
const countries = ["Argentina","Austria","Brazil","Canada","Finland","France","Germany","Hungary","Ireland","Italy","Lithuania","Mexico","Netherlands","New Zealand","Norway","Peru","Portugal","Russia","Spain","Sweden","United Kingdom","United States of America"]

function CitySelection(props: any) {
    const { fahren, setFahren, state, setState, country, setCountry, setSearch, search, setCities, sortOrder, setSortOrder, itemsPerPage, page, setCount, setPage, sort, setSort } = props;
    
    const history = useHistory();

    useEffect(() => {
        history.push("/cities?query=" + search +  "&state=" + state.join(',') + "&country=" + country.join(',') +  "&sort=" + sort+ "&order=" + sortOrder + "&page=" + page + "&step=" + itemsPerPage);
    }, [itemsPerPage, page, sort, sortOrder, state, country, search])

    const handleTemp = (event: React.ChangeEvent<HTMLInputElement>) => {
      setFahren(event.target.checked);
    };
    

    const handleChange = (event) => {
        const {
          target: { value },
        } = event;
        setCountry(
          typeof value === 'string' ? value.split(',') : value,
        );
      };

      const handleChangeState= (event) => {
        const {
          target: { value },
        } = event;
        setState(
          typeof value === 'string' ? value.split(',') : value,
        );
      };

    const handleSortOrder = (event: any) => {
        setPage(0)
        setSortOrder(event.target.value);
    };

    const handleSort = (event: any) => {
        setPage(0)
        setSort(event.target.value);
    };
    const handleSearch = (event: any) => {
        setSearch(event.target.value.toString().replace(/[^\w\s]/gi, ''));
    };
    const validate = (event: any) => {
        if (event.key === 'Enter'){
            test(event)
        }
    }

    const test = (event: any) => {
        setPage(0)
        getCities(page, itemsPerPage, search, state, country, sort, sortOrder)
        .then(response => {
            setCities(response['results']);
            setCount(response['count']);
        })
        history.push("/cities?query=" + search +  "&state=" + state.join(',') + "&country=" + country.join(',') +  "&sort=" + sort+ "&order=" + sortOrder + "&page=" + page + "&step=" + itemsPerPage);
    }

    return (
    <div>
        <Grid container justifyContent="center" mt={6}>
            <Paper
            component="form"
            sx={{ p: '2px 4px', display: 'flex', alignItems: 'center', width: 1000 }}
            >
            <InputBase
                sx={{ ml: 1, flex: 1 }}
                placeholder="Search"
                inputProps={{ 'aria-label': 'search' }}
                value={search}
                onChange={handleSearch}
                onKeyDown={validate}
            />
            <IconButton onClick={test} sx={{ p: '10px' }} aria-label="search">
                <SearchIcon />
            </IconButton>
            <Divider sx={{ height: 35, m: 0.5 }} orientation="vertical" />
            <FormControl sx={{m: 1, width: 150 }} variant="standard">
                <InputLabel id="demo-customized-select-label">Country</InputLabel>
                <Select
                labelId="demo-multiple-checkbox-label"
                id="demo-multiple-checkbox"
                multiple
                value={country}
                onChange={handleChange}
                renderValue={(selected) => selected.join(', ')}
                MenuProps={CountryMenuProps}
                >
                    {countries.map((name) => (
                        <MenuItem key={name} value={name}>
                        <Checkbox checked={country.indexOf(name) > -1} />
                        <ListItemText primary={name} />
                        </MenuItem>
                    ))}
                </Select>
            </FormControl>
            <FormControl sx={{m: 1, width: 200 }} variant="standard">
                <InputLabel id="demo-customized-select-label">State</InputLabel>
                <Select
                labelId="demo-multiple-checkbox-label"
                id="demo-multiple-checkbox"
                multiple
                value={state}
                onChange={handleChangeState}
                renderValue={(selected) => selected.join(', ')}
                MenuProps={MenuProps}
                >
                    {states.map((name) => (
                        <MenuItem key={name} value={name}>
                        <Checkbox checked={state.indexOf(name) > -1} />
                        <ListItemText primary={name} />
                        </MenuItem>
                    ))}
                </Select>
            </FormControl>
            <FormControl sx={{ m: 1, minWidth: 120 }} variant="standard">
                <InputLabel id="demo-customized-select-label">Sort By</InputLabel>
                <Select
                labelId="demo-simple-select-error-label"
                id="demo-simple-select-error"
                value={sort}
                onChange={handleSort}
                >
                <MenuItem value="city">
                    <em>City</em>
                </MenuItem>
                <MenuItem value={"population"}>Population</MenuItem>
                <MenuItem value={"temperature"}>Temperature</MenuItem>
                </Select>
            </FormControl>
            <FormControl sx={{ m: 1, minWidth: 120 }} variant="standard">
                <InputLabel id="demo-customized-select-label">Sort Order</InputLabel>
                <Select
                labelId="demo-simple-select-error-label"
                id="demo-simple-select-error"
                value={sortOrder}
                onChange={handleSortOrder}
                >
                <MenuItem value="asc">
                    <em>Ascending</em>
                </MenuItem>
                <MenuItem value={"desc"}>Descending</MenuItem>
                </Select>
            </FormControl>
            <FormControlLabel control={<Switch checked={fahren} onChange={handleTemp} defaultChecked />} label={fahren ? "°F" : "°C"} />
        </Paper>
    </Grid>
    </div>
  );
}

export default CitySelection;