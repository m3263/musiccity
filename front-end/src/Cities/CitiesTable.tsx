import React from 'react';
import { Link } from 'react-router-dom';
import { TableCell, TableBody, TableRow, Table, TableHead, TableContainer } from '@mui/material/';
import { FtoC } from '../functions/formatting'

var Highlight = require('react-highlighter');

function CitiesTable(props: any) {
    const { fahren, cities, search } = props;

    const pattern = new RegExp(search.trim().replace(" ", "|").replace("$", "\\$"), "i")

    return (
        <TableContainer>
            <Table stickyHeader aria-label="sticky table">
                <TableHead>
                    <TableRow>
                        <TableCell>
                            City
                        </TableCell>
                        <TableCell>
                            State
                        </TableCell>
                        <TableCell>
                            Country
                        </TableCell>
                        <TableCell>
                            Population
                        </TableCell>
                        <TableCell>
                            Avg. Temperature {fahren ? "°F" : "°C"}
                        </TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {cities.map((row: any) => {
                        return (
                            <TableRow hover role="checkbox" key={row.id} component={Link} to={"/cities/" + row.id} style={{ textDecoration: 'none' }}>
                                <TableCell>
                                    <Highlight style={{ fontSize: '20px' }} search={search === "" ? "" : pattern}>{row.name}</Highlight>
                                </TableCell>
                                <TableCell>
                                    <Highlight style={{ fontSize: '20px' }} search={search === "" ? "" : pattern}>{row.state}</Highlight>
                                </TableCell>
                                <TableCell>
                                    <Highlight style={{ fontSize: '20px' }} search={search === "" ? "" : pattern}>{row.country}</Highlight>
                                </TableCell>
                                <TableCell>
                                    <Highlight style={{ fontSize: '20px' }} search={search === "" ? "" : pattern}>{row.population.toString()}</Highlight>
                                </TableCell>
                                <TableCell>
                                    <Highlight style={{ fontSize: '20px' }} search={search === "" ? "" : pattern}>{fahren ? Number(row.temperature).toFixed(2).toString() : FtoC(Number(row.temperature)).toFixed(2).toString()}</Highlight>
                                </TableCell>
                            </TableRow>
                        );
                    })}
                </TableBody>
            </Table>
        </TableContainer>
    );
}
export default CitiesTable;