import React, { useState, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { Grid, Typography, Card, CardMedia, CardContent, CardActionArea, Paper, InputBase } from '@mui/material/';
import IconButton from '@mui/material/IconButton';
import SearchIcon from '@mui/icons-material/Search';

function Splash({location}) {
    let params = new URLSearchParams(location.search);
    const history = useHistory();
    const [search, setSearch] = useState(params.get("query") !== null ? params.get("query") : "")
    
    const validate = (event: any) => {
        if (event.key === 'Enter'){
            event.preventDefault() 
            test(event)
        }
    }

    const test = (event: any) => {
        history.push("search?query=" + search);
    }

    const handleSearch = (event: any) => {
        setSearch(event.target.value);
    };

    return (
        <Grid container rowSpacing={5} columnSpacing={5} alignItems="center" justifyContent="center" p={10}>
            <Grid item xs={12}>
                <Card>
                    <CardMedia
                    component="img"
                    image="/images/firstLogo.png"
                    alt="music city"
                    />
                    <CardContent>
                        <Typography gutterBottom variant="h3" component="div" align='center'>
                            Welcome to Music City!
                        </Typography>
                        <Typography variant="h4" color="text.secondary" align='center'>
                            Find Artists Perfoming in your City
                        </Typography>
                    </CardContent>
                </Card>
            </Grid>
            <Grid item xs={12}>
                <Typography gutterBottom variant="h4" component="div" align='center'>
                    Search All
                </Typography>
                <Grid container justifyContent="center">
                    <Paper
                        component="form"
                        sx={{ p: '2px 4px', display: 'flex', alignItems: 'center', width: 800 }}
                    >
                        <InputBase
                            sx={{ ml: 1, flex: 1 }}
                            placeholder="Search"
                            inputProps={{ 'aria-label': 'search' }}
                            value={search}
                            onChange={handleSearch}
                            onKeyDown={validate}
                        />
                        <IconButton onClick={test} sx={{ p: '10px' }} aria-label="search">
                            <SearchIcon />
                        </IconButton>
                    </Paper>
                </Grid>
            </Grid>
            <Grid item xs={4}>
                <CardActionArea component={Link} to={{pathname:`/cities`}} >
                    <Card sx={{ height: 155}}>
                        <CardContent>
                            <Typography gutterBottom variant="h4" component="div" align='center'>
                                Search by City
                            </Typography>
                            <Typography gutterBottom variant="subtitle1" component="div" align='center'>
                                Get information on live events, artists performances, and other things to do!
                            </Typography>
                        </CardContent>
                    </Card>
                </CardActionArea>
            </Grid>
            <Grid item xs={4}>
                <CardActionArea component={Link} to={{pathname:`/artists`}}>
                    <Card sx={{ height: 155}}>
                        <CardContent>
                            <Typography gutterBottom variant="h4" component="div" align='center'>
                                Search by Artist
                            </Typography>
                            <Typography gutterBottom variant="subtitle1" component="div" align='center'>
                                Find new artists and check out their upcoming live events.
                            </Typography>
                        </CardContent>
                    </Card>
                </CardActionArea>
            </Grid>
            <Grid item xs={4}>
                <CardActionArea component={Link} to={{pathname:`/events`}}>
                    <Card sx={{ height: 155}}>
                        <CardContent>
                            <Typography gutterBottom variant="h4" component="div" align='center'>
                                Search by Event
                            </Typography>
                            <Typography gutterBottom variant="subtitle1" component="div" align='center'>
                                Find new shows near you and at your price point.
                            </Typography>
                        </CardContent>
                    </Card>
                </CardActionArea>
            </Grid>
        </Grid>
    );
}

export default Splash;