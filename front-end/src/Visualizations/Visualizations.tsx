import React, { useEffect, useState } from 'react';
import { Grid, Box, CircularProgress, Typography } from '@mui/material/';
import { getFollowerData, getCityEvent } from '../functions/fetchVisualizationData'
import { getEvents } from '../functions/fetchEventData'
import { Radar, RadarChart, PolarGrid, Legend, PolarAngleAxis, PolarRadiusAxis, ResponsiveContainer } from 'recharts';
import BubbleChart from './BubbleChart';
import BubbleMap from './BubbleMap';
import { getCities, getCityData } from '../functions/fetchCityData'

function Visualizations(props: any) {
    const [followers, setFollowers] = useState([]);
    const [events, setEvents] = useState([]);
    const [cities, setCities] = useState([]);
    const [loading, setLoading] = useState(false);
    const [eventsLoading, setEventsLoading] = useState(false);
    const [citiesLoading, setCitiesLoading] = useState(false);

    function getLinks(id, result, citiesData, i) {
        return new Promise(async function(resolve, reject) {
            await new Promise(res => setTimeout(res, i));
            getCityData(id)
            .then(response => {
                var name = response['data']['name']
                result.push({
                    'City': citiesData[name],
                    'lat': citiesData[name]['lat'],
                    'long': citiesData[name]['long'],
                    'country': citiesData[name]['country'],
                    'value': response['data']['links'].length
                })
                resolve(null)
            })
        });
    }

    // useEffect(() => {
    //     setCitiesLoading(true)
    //     getCities(0, 141, "", [], [], 'city', 'asc')
    //     .then(response => {
    //         var data = response['results']
    //         var cities = {}
              
    //         var result = []
    //         var promises = []

    //         for (var i = 0; i < data.length; i++) {
    //             cities[data[i]['name']] = {
    //                 'City': data[i]['name'], 
    //                 'lat': data[i]['latitude'],
    //                 'long': data[i]['longitude'],
    //                 'country': data[i]['country'],
    //             }
    //             promises.push(getLinks(data[i]['id'], result, cities, i))
    //         }
    //         Promise.all(promises)
    //         .then(response => {
    //             setCities(result)
    //             setCitiesLoading(false)
    //         })
    //     })
    // }, []);

    useEffect(() => {
        setCitiesLoading(true)
        getCityEvent()
        .then(response => {
            var data = response['results']
            var cities = {}
            var result = []

            for (var i = 0; i < data.length; i++) {
                var id = data[i]['Cities']['id']
                if (id in cities) {
                    cities[id]['value'] += 1
                }
                else {
                    cities[id] = {
                        'City': data[i]['Cities']['name'],
                        'lat': data[i]['Cities']['latitude'],
                        'long': data[i]['Cities']['longitude'],
                        'country': data[i]['Cities']['country'],
                        'value': 1
                    }
                }
            }
            var keys = Object.keys(cities)
            for (var i = 0; i < keys.length; i++) {
                result.push(cities[keys[i]])
            }
            setCities(result)
            setCitiesLoading(false)
        })
    }, []);


    useEffect(() => {
        setEventsLoading(true)
        getEvents(0, 418)
        .then(response => {
            var data = response['data']['results']
            var venues = {}
            for (var i = 0; i < data.length; i++) {
                if (!(data[i]['venue_name'] in venues)) {
                    venues[data[i]['venue_name']] = 1
                }
                else {
                    venues[data[i]['venue_name']] += 1
                }
            }
            var result = []
            var places = Object.keys(venues)
            for (var i = 0; i < places.length; i++) {
                if (venues[places[i]] > 2) {
                    result.push({
                        'Name': places[i],
                        'Count': venues[places[i]]
                    })
                }
            }
            setEvents(result)
            setEventsLoading(false)
        })
    }, []);

    useEffect(() => {
        setLoading(true)
        getFollowerData(1000)
        .then(response => {
            var dict = {}
            for (var i = 0; i < response.length; i++) {
                var f = response[i]['followers']
                for (var j = 0; j < response[i]['genres'].length; j++) {
                    var genre = response[i]['genres'][j]
                    if (!(genre in dict)) {
                        dict[genre] = {
                            'followers': f,
                            'artists': 1
                        }
                    }
                    else {
                        dict[genre]['followers'] += f
                        dict[genre]['artists'] += 1
                    }
                }
            }
            var keys = Object.keys(dict)
            var data = []
            for (var i = 0; i < keys.length; i++) {
                if (dict[keys[i]]['artists'] > 7) {
                    data.push({
                        'genre': keys[i],
                        'followers': dict[keys[i]]['followers'] / 20000000,
                        'artists': dict[keys[i]]['artists'] 
                    })
                }
            }
            setFollowers(data)
            setLoading(false)
        })
    }, []);

    return (
        <Grid container alignItems="center" justifyContent="center">
            <Grid mt={5} container justifyContent='center' alignItems='center' sx={{ maxWidth: "80%" }}>
                <Grid item xs={12}>
                    <Typography variant='h4'>
                        Visualizations
                    </Typography>
                </Grid>
                <Grid item xs={12}>
                    <Typography variant='h5' mt={5}>
                        Total Number of Followers and Artists for Top Genres
                    </Typography>
                    {loading ? (
                        <Box mt={5} sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                            <CircularProgress />
                        </Box>
                    ) : (
                        <ResponsiveContainer width="100%" height={500}>
                            <RadarChart outerRadius={150} data={followers}>
                            <PolarGrid />
                            <PolarAngleAxis dataKey="genre" />
                            <PolarRadiusAxis angle={45} />
                            <Radar name="Total Followers(20 Millions)" dataKey="followers" stroke="#8884d8" fill="#8884d8" fillOpacity={0.8} />
                            <Radar name="Number of Artists" dataKey="artists" stroke="#82ca9d" fill="#82ca9d" fillOpacity={0.3} />
                            <Legend />
                            </RadarChart>
                        </ResponsiveContainer>
                    )}
                </Grid>
                <Grid item xs={12}>
                    <Typography variant='h5' mt={5}>
                        Frequency of Events at each Venue
                    </Typography>
                    {eventsLoading ? (
                        <Box mt={5} sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                            <CircularProgress />
                        </Box>
                    ) : (
                        <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                            <BubbleChart events={events}/>
                        </Box>
                    )}
                </Grid>
                <Grid item xs={12}>
                    <Typography variant='h5' mt={5} mb={5}>
                        Number of Events in each City
                    </Typography>
                    {citiesLoading ? (
                        <Box mt={5} sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                            <CircularProgress />
                        </Box>
                    ) : (
                        <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center'}} mb={10}>
                            <BubbleMap cities={cities}/>
                        </Box>
                    )}
                </Grid>
            </Grid>
        </Grid>
    );
}

export default Visualizations;
