import React from 'react';
import * as d3 from "d3";

export default class BubbleMap extends React.Component<any, any> {
    private readonly v1Ref = React.createRef<SVGSVGElement>();

    constructor(props) {
        super(props);

    }
    async renderChart(cities: any) {
        const svg = d3.select("#my_dataviz"),
        width = +svg.attr("width"),
        height = +svg.attr("height");
    
        // Map and projection
        const projection = d3.geoMercator()
            .center([0,20])                // GPS of location to zoom on
            .scale(99)                       // This is like the zoom
            .translate([ width/2, height/2 ])
        
        Promise.all([
        d3.json("https://raw.githubusercontent.com/holtzy/D3-graph-gallery/master/DATA/world.geojson")
        ]).then(function (initialize) {
    
            let dataGeo = initialize[0]
            let data = cities
    
            // Create a color scale
            const color = d3.scaleOrdinal()
                .domain(data.map(d => d.country))
                .range(d3.schemePaired);
        
            // Add a scale for bubble size
            const valueExtent = d3.extent(data, d => +d.value)
            const size = d3.scaleSqrt()
            .domain(valueExtent)  // What's in the data
            .range([ 1, 50])  // Size in pixel

            svg.append("g")
            .selectAll("path")
            .data(dataGeo.features)
            .join("path")
            .attr("fill", "#b8b8b8")
            .attr("d", d3.geoPath()
                .projection(projection)
            )
            .style("stroke", "none")
            .style("opacity", .3)

            svg
            .selectAll("myCircles")
            .data(data.sort((a,b) => +b.value - +a.value).filter((d,i) => i<1000))
            .join("circle")
            .attr("cx", d => projection([+d.long, +d.lat])[0])
            .attr("cy", d => projection([+d.long, +d.lat])[1])
            .attr("r", d => size(+d.value))
            .style("fill", d => color(d.country))
            .attr("stroke", d=> {if (d.value>2000) {return "black"} else {return "none"}  })
            .attr("stroke-width", 1)
            .attr("fill-opacity", .4)

            const valuesToShow = [1,10,20]
            const xCircle = 40
            const xLabel = 90
            svg
            .selectAll("legend")
            .data(valuesToShow)
            .join("circle")
            .attr("cx", xCircle)
            .attr("cy", d => height - size(d))
            .attr("r", d => size(d))
            .style("fill", "none")
            .attr("stroke", "black")
        
            // Add legend: segments
            svg
            .selectAll("legend")
            .data(valuesToShow)
            .join("line")
            .attr('x1', d => xCircle + size(d))
            .attr('x2', xLabel)
            .attr('y1', d => height - size(d))
            .attr('y2', d => height - size(d))
            .attr('stroke', 'black')
            .style('stroke-dasharray', ('2,2'))

            svg
            .selectAll("legend")
            .data(valuesToShow)
            .join("text")
            .attr('x', xLabel)
            .attr('y', d => height - size(d))
            .text(d => d)
            .style("font-size", 10)
            .attr('alignment-baseline', 'middle')
        })
    }

    async componentDidMount() {

        await this.renderChart(this.props.cities);

    }

    render() {
        return (
            <div>
                <div>
                    <svg id="my_dataviz" width="700" height="400"></svg>
                </div>
            </div>
        );
    }

}