import React from 'react';
import * as d3 from "d3";
import { schemeCategory10 } from "d3-scale-chromatic"
import { formatStr } from '../functions/formatting'

export default class BubbleChart extends React.Component<any, any> {
    private readonly v1Ref = React.createRef<SVGSVGElement>();

    constructor(props) {
        super(props);

    }
    async renderChart(events: any) {
        if (events['children'].length > 0) {
            var diameter = 700;
            var color = d3.scaleOrdinal(schemeCategory10);

            var bubble = d3.pack(events)
                .size([diameter, diameter])
                .padding(1.5);

            var svg = d3.select("#map")
                .append("svg")
                .attr("width", diameter)
                .attr("height", diameter)
                .attr("class", "bubble");

            var nodes = d3.hierarchy(events)
                .sum(function(d) { return d.Count; });

            var node = svg.selectAll(".node")
                .data(bubble(nodes).descendants())
                .enter()
                .filter(function(d){
                    return  !d.children
                })
                .append("g")
                .attr("class", "node")
                .attr("transform", function(d) {
                    return "translate(" + d.x + "," + d.y + ")";
                });

            node.append("title")
                .text(function(d) {
                    return d.Name + ": " + d.Count;
                });

            node.append("circle")
                .attr("r", function(d) {
                    return d.r;
                })
                .style("fill", function(d,i) {
                    return color(i);
                });

            

            node.append("text")
                .attr("dy", ".2em")
                .style("text-anchor", "middle")
                .text(function(d) {
                    return formatStr(d.data.Name, (d.r / 15) * 5);
                })
                .attr("font-family", "sans-serif")
                .attr("font-size", function(d){
                    return d.r/5;
                })
                .attr("fill", "white");

            node.append("text")
                .attr("dy", "1.3em")
                .style("text-anchor", "middle")
                .text(function(d) {
                    return d.data.Count;
                })
                .attr("font-family",  "Gill Sans", "Gill Sans MT")
                .attr("font-size", function(d){
                    return d.r/5;
                })
                .attr("fill", "white");

            d3.select("#map")
                .style("height", diameter + "px");
        }
    }

    async componentDidMount() {

        await this.renderChart({'children': this.props.events});

    }

    render() {
        return (
            <div>
                <div>
                    <div className="view" id="map"></div>
                </div>
            </div>
        );
    }

}