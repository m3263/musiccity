import React from 'react';
import { Box, Typography, Card } from '@mui/material/';
import { ScatterChart, Scatter, XAxis, YAxis, CartesianGrid, Tooltip, Cell, ResponsiveContainer } from 'recharts';
import { interpolateWarm } from "d3-scale-chromatic";

const CustomTooltip = props => {
    if (props.active && props.payload && props.payload.length) {
        return (
        <Card>
            <Box ml={1}>
                <Typography variant='subtitle1'>
                    {`${props.payload[1].payload.name}`}
                </Typography>
                <Typography variant='subtitle1'>
                    {`Price: $${props.payload[0].value}`}
                </Typography>
                <Typography variant='subtitle1'>
                    {`Protein Percentage: ${props.payload[1].value}%`}
                </Typography>
            </Box>
        </Card>
        );
    }

    return null;
};

const PriceLabel = props => {
    const { x, y, width, height } = props.viewBox;
    return (
        <text x={x + (width / 2)} y={y + 50} dy={-4} fontSize={16} textAnchor="middle">Price</text>
    );
  };

const ProteinLabel = props => {
    const { x, y, width, height } = props.viewBox;
    return (
            <text style={{transform: 'rotate(-90deg)'}} x={x - (height / 2)} y={y} dy={-4} fontSize={16} textAnchor="middle">Percentage Protein</text>
    );
};

function IngredientChart(props: any) {
    const { proteinIngredients, count } = props;


    return (
        <ResponsiveContainer width="100%" height={500}>
            <ScatterChart
            width={400}
            height={400}
            margin={{
                top: 20,
                right: 20,
                bottom: 20,
                left: 20,
            }}
            >
            <CartesianGrid />
            <XAxis type="number" dataKey="price" label={PriceLabel} unit="$" />
            <YAxis type="number" dataKey="protein" label={ProteinLabel} unit="%" />
            <Tooltip content={<CustomTooltip />} />
            <Scatter name="A school" data={proteinIngredients} fill="#8884d8">
                {proteinIngredients.map((entry, index) => {
                return (
                    <Cell key={`cell-${index}`} fill={interpolateWarm((1/count) * index)} />
                );
                })}
            </Scatter>
            </ScatterChart>
        </ResponsiveContainer>
    );
}

export default IngredientChart;