import React, { useEffect, useState } from 'react';
import * as d3 from "d3";
import { Grid, Box, CircularProgress, Typography } from '@mui/material/';
import { getHighProteinIngredients, getRecipies, getCuisines } from '../functions/fetchProviderData'
import AllergenChart from './AllergenChart'
import IngredientChart from './IngredientChart';
import CuisineChart from './CuisineChart'
import countries from '../data/countries'

function ProviderVisualizations(props: any) {
    const [proteinIngredients, setProteinIngredients] = useState([]);
    const [allergens, setAllergens] = useState([]);
    const [cuisines, setCuisines] = useState([]);
    const [proteinLoading, setProteinLoading] = useState(false);
    const [recipeLoading, setRecipeLoading] = useState(false);
    const [cuisineLoading, setCuisineLoading] = useState(false);

    const count = 45;


    useEffect(() => {
        setProteinLoading(true)
        getHighProteinIngredients(count)
        .then(response => {
            var data = []
            for (var i = 0; i < response.length; i++) {
                var nutrients = JSON.parse(response[i]['macronutrients'])
                data.push({
                    'name': response[i]['name'],
                    'price': response[i]['estimated_cost'],
                    'protein': nutrients['percentProtein'] 
                })
            }
            setProteinIngredients(data)
            setProteinLoading(false)
        })
    }, []);

    useEffect(() => {
        setCuisineLoading(true)
        getCuisines()
        .then(response => {
            var data = {}
            for (var i = 0; i < response.length; i++) {
                var country = response[i]['subregion']
                if (country in data) {
                    data[country] += 1
                }
                else {
                    data[country] = 1
                }
            }
            var result = []
            for (var i = 0; i < Object.keys(data).length; i++) { 
                result.push({
                    'name': Object.keys(data)[i],
                    'set': countries[Object.keys(data)[i]],
                    'value': data[Object.keys(data)[i]],
                })
            }
            setCuisines(result)
            setCuisineLoading(false)
        })
    }, []);

    useEffect(() => {
        setRecipeLoading(true)
        getRecipies()
        .then(response => {
            var data = {}
            for (var i = 0; i < response.length; i++) {
                var allergens = response[i]['common_allergens']
                allergens = allergens.substring(1, allergens.length - 1).split(",")
                for (var j = 0; j < allergens.length; j++) {
                    if (allergens[j] in data) {
                        data[allergens[j]] += 1
                    }
                    else {
                        data[allergens[j]] = 1
                    }
                }
            }
            var result = []
            for (var i = 0; i < Object.keys(data).length; i++) { 
                result.push({
                    'name': Object.keys(data)[i],
                    'value': data[Object.keys(data)[i]],
                    'percent': (data[Object.keys(data)[i]] / response.length) * 100
                })
            }
            setAllergens(result)
            setRecipeLoading(false)
        })
    }, []);

    return (
        <Grid container alignItems="center" justifyContent="center">
            <Grid mt={5} container justifyContent='center' alignItems='center' sx={{ maxWidth: "80%" }}>
                <Grid item xs={12}>
                    <Typography variant='h4'>
                        Provider Visualizations
                    </Typography>
                </Grid>
                <Grid item xs={12}>
                    <Typography variant='h5' mt={5}>
                        Lowest Cost High Protein Ingredients
                    </Typography>
                        {proteinLoading ? (
                            <Box mt={5} sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                                <CircularProgress />
                            </Box>
                        ) : (
                            <IngredientChart count={count} proteinIngredients={proteinIngredients} />
                        )}
                </Grid>
                <Grid item xs={12}>
                    <Typography variant='h5' mt={5}>
                        Frequency of Allergen-Free Recipies
                    </Typography>
                        {recipeLoading ? (
                            <Box mt={5} sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                                <CircularProgress />
                            </Box>
                        ) : (
                            <AllergenChart allergens={allergens} />
                        )}
                </Grid>
                <Grid item xs={12}>
                    <Typography variant='h5' mt={5}>
                        Number of Cuisines per Region
                    </Typography>
                        {cuisineLoading ? (
                            <Box mt={5} sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                                <CircularProgress />
                            </Box>
                        ) : (
                            <CuisineChart cuisines={cuisines} />
                        )}
                </Grid>
            </Grid>  
        </Grid>
    );
}

export default ProviderVisualizations;
