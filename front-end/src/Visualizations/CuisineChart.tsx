import React from 'react';
import * as d3 from "d3";
import * as topojson from "topojson-client";
import "./CuisineChart.css";

export default class CusineChart extends React.Component<any, any> {
    private readonly v1Ref = React.createRef<SVGSVGElement>();

    constructor(props) {
        super(props);

        this.state = {
            loading: true
        };
    }

    async renderVisualizationOne(cuisines: any) {
        (document.querySelector('.legend') as HTMLElement).innerText = 'Hover mouse over region';
        d3.select(window).on("resize", resize);

        var width = (document.querySelector('#map') as HTMLElement).offsetWidth;
        var mapRatio = 0.7;
        var height = width * mapRatio;

        var projection = d3.geoMercator()
            .scale(width / 2 / Math.PI)
            .rotate([-11, 0])
            .translate([(width) / 2, height * 1.35 / 2])
            .precision(.1);

        var path = d3.geoPath().projection(projection);

        var svg = d3.select("#map")
                .append("svg")
                .attr("preserveAspectRatio", "xMinYMin")
                .attr("width", width)
                .attr("height", height)
                .append("g");

        d3.json("world.json")
        .then(function(w) {
    
            svg.append("path").datum(topojson.merge(w, w.objects.units.geometries.filter(function (d) {
                        return d.id !== 'ATA';
                    })))
                    .attr("class", "border")
                    .attr("d", path);
    
            for (var i = 0; i < cuisines.length; i++) {
                svg.append("path").datum(topojson.merge(w, w.objects.units.geometries.filter(function (d) {
                            return cuisines[i].set.has(d.id);
                        })))
                        .attr('class', "regions selected")
                        .attr("d", path)
                        .attr('data-name', cuisines[i].name + ": " + cuisines[i].value + " different cuisines")
                        .on('mouseover', function () {
                            var region = d3.select(this);
                            (document.querySelector('.legend') as HTMLElement).innerText = region.attr('data-name');
                        }).on('mouseout', function () {
                            (document.querySelector('.legend') as HTMLElement).innerText = 'Hover mouse over region';
                        });
            }
    
        })
        .catch((function(error) {
            console.log(error)
          }));

        function resize() {
            width = (document.querySelector('#map') as HTMLElement).offsetWidth;
            height = width * mapRatio;
    
            projection.scale(width / 2 / Math.PI)
                    .translate([(width) / 2, height * 1.35 / 2])
                    .precision(.1);

            document.querySelector('svg').setAttribute('width', width.toString());
            document.querySelector('svg').setAttribute('height', height.toString());
    
            svg.selectAll('.regions, .border').attr('d', path);
        }
    }

    async componentDidMount() {

        await this.renderVisualizationOne(this.props.cuisines);

        await this.setState({
            loading: false
        });
    }

    render() {
        return (
            <div>
                <div>
                    {this.state.loading && (
                        <div>
                            <div className="visualization-loading-circle"></div>
                        </div>
                    )}
                    <div className="legend"></div>
                    <div className="view" id="map"></div>
                </div>
            </div>
        );
    }

}
