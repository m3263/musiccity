

interface EventInterface {
    id: string

    artist_name: string;
    artist_image_url: string;
    facebook_page_url: string;

    event_url: string;
    sale_time: string;
    date: string;
    description: string;
    title: string;
    country: string;
    venue_name: string;
    latitude: number;
    longitude: number;
    city: string;
    lineup: Array<string>;
}


export default EventInterface;