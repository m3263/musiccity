import React, { useState, useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import { Paper, Box } from "@material-ui/core";
import { Grid, CircularProgress } from '@mui/material';
import CustomPagination from '../shared_components/CustomPagination';
import { getEventsSearch } from '../functions/fetchEventData'
import EventInterface from "./EventInterface"
import EventSearchBar from "./EventSearchBar"
import EventsTable from "./EventsTable"

//TODO Pagination, Update looks and look at phase 1 submission statement
function EventsModel({location}) {
    let params: any = new URLSearchParams(location.search);
    const [allEvents, setEvents]: [any, any] = useState([]);
    const [page, setPage] = useState(params.get("page") === null ? 0 : parseInt(params.get("page")));
    const [search, setSearch] = useState(params.get("query") === null ? "" : params.get("query"));
    const [itemsPerPage, setItemsPerPage] = useState(params.get("step") === null ? 10 : parseInt(params.get("step")));
    const[sortOrder, setSortOrder] = useState(params.get("order") === null ? 'asc' : params.get("order"));
    const [sortBy, setSortBy] = useState(params.get("sort") === null ? 'time' : params.get("sort"));
    const [count, setCount] = useState(10);
    const [loading, setLoading] = useState<boolean>(true);
    var data = allEvents;

    useEffect(() => {
        getData();
    }, [page, itemsPerPage, sortBy, sortOrder]);

    const getData = async () => {
        const eventData = await getEventsSearch(page, itemsPerPage, sortBy, search, sortOrder)

        let tableRows = [];
        if (eventData) {
            let length = eventData.data['count'];
            const rowData = eventData.data['results'];
            console.log("refreshing, grabbing", length, "instances: first row", rowData[0], " page is : ", page);
            setEvents(rowData);
            setCount(length);
            setLoading(false);
        }
        else {
            setCount(0);
        }
    }

    return (
        <Grid container alignItems="center" justifyContent="center"> 
            <Grid container rowSpacing={5} alignItems="center" justifyContent="center" sx={{ maxWidth: "80%" }}>
                <Grid item xs={12}>
                    <EventSearchBar
                        search={search}
                        setSearch={setSearch}
                        setEvents={setEvents}
                        sortOrder={sortOrder}
                        setSortOrder={setSortOrder}
                        pageSize={itemsPerPage}
                        page={page}
                        setCount={setCount}
                        setPage={setPage}
                        sortBy={sortBy}
                        setSortBy={setSortBy}
                    />
                </Grid>
                <Grid item xs={12}>
                    {loading ? (
                            <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center'}} mt={10}>
                                <CircularProgress />
                            </Box>
                        ) : (
                            <Grid item xs={12}>
                               <EventsTable events={allEvents} search={search}/>
                            </Grid>
                        )}
                </Grid>
                <Grid item xs={12}>
                    <Grid container alignItems="center" justifyContent="center">
                        <CustomPagination page={page} setPage={setPage} itemsPerPage={itemsPerPage} setItemsPerPage={setItemsPerPage} count={count} />
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    );
}


export default EventsModel;