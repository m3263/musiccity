import React from 'react';
import { Link } from 'react-router-dom';
import { formatDate, formatTime } from "../functions/formatting"
import { TableCell, TableBody, TableRow, Table, TableHead, TableContainer } from '@mui/material/';

var Highlight = require('react-highlighter');

function EventsTable(props: any) {
    const { events, search } = props;

    const pattern = new RegExp(search.trim().replace(" ", "|").replace("$", "\\$"), "i")

    if (events) {
        return (
            <TableContainer>
                <Table stickyHeader aria-label="sticky table">
                    <TableHead>
                        <TableRow>
                            <TableCell align="center">Featured Artist</TableCell>
                            <TableCell align="center">Venue</TableCell>
                            <TableCell align="center">City</TableCell>
                            <TableCell align="center">Start Time</TableCell>
                            <TableCell align="center">Country</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {events.map((row) => (
                            <TableRow hover role="checkbox" tabIndex={-1} key={row.id} component={Link} to={"/events/" + row.id} style={{ textDecoration: 'none' }} >
                                <TableCell component="th" scope="row"><Highlight style={{ fontSize: '20px' }} search={search === "" ? "" : pattern}>{row.artist_name}</Highlight></TableCell>
                                <TableCell align="left"><Highlight style={{ fontSize: '20px' }} search={search === "" ? "" : pattern}>{row.venue_name}</Highlight></TableCell>
                                <TableCell align="left"><Highlight style={{ fontSize: '20px' }} search={search === "" ? "" : pattern}>{row.city}</Highlight></TableCell>
                                <TableCell align="left"><Highlight style={{ fontSize: '20px' }} search={search === "" ? "" : pattern}>{formatDate(row.date)}</Highlight></TableCell>
                                <TableCell align="left"><Highlight style={{ fontSize: '20px' }} search={search === "" ? "" : pattern}>{row.country}</Highlight></TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        );
    }
    return <div/>
}

export default EventsTable;