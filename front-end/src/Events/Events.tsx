import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import Typography from '@mui/material/Typography';
import { Card, CardContent, CardMedia, Box, Grid, CardActionArea } from '@mui/material/';
import { makeStyles, Button } from "@material-ui/core";
//import green from './statusImgs/ok.png';
//import yellow from './statusImgs/postponed.png';
//import red from './statusImgs/cancelled.png';
import { useParams } from 'react-router-dom'
import ReactPlayer from 'react-player'
import { styled, createTheme, ThemeProvider } from '@mui/system';
import { formatTime } from "../functions/formatting"
import { getEventData, getEventLinks } from '../functions/fetchEventData'

//function getStatusImg(status: string) {
//    if (status === ("ok")) {
//        return green;
//    }
//    else if (status === ("postponed")){
//        return yellow;
//    }
//    else if (status === ("cancelled")){
//        return red;
//    }
//}

const useStyles = makeStyles((theme) => ({
	link: {
	  color: "green",
	  fontSize: "18px",
	  textTransform: 'none',
	},
  }));

function Events() {
    let params: any = useParams();
    const eventId = params.id;
    const [eventLinks, setLinks]: [any, any] = useState([]);
    const [jsonData, setEventData]: [any, any] = useState([]);
    const [loading, setLoading] = useState<boolean>(true);
    const classes = useStyles();

    useEffect(() => {
        getData();
    }, []);

    const getData = async () => {
        const eventData = await getEventData(eventId);
        const linkData = await getEventLinks(eventId);
        console.log(linkData.data[0])
        setEventData(eventData.data);
        setLinks(linkData.data[0]);
        setLoading(false);
    }
    if (!loading) {
        return (
            <div>
                <Grid item xs={12} justifyContent="center">
                    <Typography variant="h1" component="div">{jsonData.artist_name}</Typography>
                    <Card sx={{ bgcolor: 'primary.secondary' }}>
                        <CardContent>
                            <Typography variant="h3" component="div">{jsonData.venue_name}</Typography>
                            <Typography variant="subtitle1" component="div">City: {jsonData.city}</Typography>
                            <Typography variant="subtitle1" component="div">Date: {formatTime(jsonData.date)}</Typography>
                            <Typography variant="subtitle1" component="div">Country: {jsonData.country}</Typography>
                            <CardMedia>
                                <img src={jsonData.artist_image_url}  width="300" height="300"  alt={jsonData.artist_image_url} />
                            </CardMedia>
                            <Typography variant="subtitle1" component="div">Tickets on Sale: {jsonData.sale_time ? formatTime(jsonData.sale_time) : "Not Available"}</Typography>
                            <Box sx={{ height: 20}}>
                                <CardActionArea href={jsonData.event_url.split('?')[0]} target="_blank">
                                Click here to view Event Page
                                </CardActionArea>
                            </Box>
                            <Box sx={{ height: 20}}>
                                <CardActionArea href={jsonData.facebook_page_url} target="_blank">
                                    Click here to view {jsonData.artist_name}'s Facebook Page
                                </CardActionArea>
                            </Box>
                            <Typography variant="subtitle1" component="div">Lineup: {jsonData.lineup.map((elem: string) => (<Typography variant="subtitle2" component="div">{"\t" + elem}</Typography>))}</Typography>
                            <Typography variant="subtitle1" component="div">Latitude: {jsonData.latitude}</Typography>
                            <Typography variant="subtitle1" component="div">Longitude: {jsonData.longitude}</Typography>
                            <iframe
                                width="450"
                                height="250"
                                frameBorder="0"
                                src={"https://www.google.com/maps/embed/v1/view?key=AIzaSyBG2KEczykPn1lvPCIc91Y93dh-7-LCzWs&center=" + jsonData.latitude + "," + jsonData.longitude + "&zoom=18&maptype=satellite"} allowFullScreen>
                            </iframe>
                            {/* <ThemeProvider theme={customTheme}>
                                <MyThemeComponent>Website URL: {jsonData.event_url}</MyThemeComponent>
                            </ThemeProvider> */}
                            <Box>
                                <Link to={"/cities/" + eventLinks.city.id} style={{ textDecoration: 'none' }}>
                                    <Button className={classes.link}>
                                    Check out other events in {eventLinks.city.name}
                                    </Button>
                                </Link>
                            </Box>
                            <Box>
                                <Link to={"/artists/" + eventLinks.artist.id} style={{ textDecoration: 'none' }}>
                                    <Button className={classes.link}>
                                    Learn more about {jsonData.artist_name}
                                    </Button>
                                </Link>
                            </Box>
                        </CardContent>
                    </Card>
                </Grid>
            </div>
        );
    }
    else {
        return (
            <div style={{ height: 1000, width: '100%' }}>
            </div>
        )
    }
}

const customTheme = createTheme({
    palette: {
      primary: {
        main: '#1976d2',
        contrastText: 'white',
        color: 'darkslategray',
        backgroundColor: 'aliceblue',
        padding: 8,
        borderRadius: 4,
      },
    },
  });
  
  const MyThemeComponent = styled('div')(({ theme }) => ({
    color: theme.palette.primary.contrastText,
    backgroundColor: theme.palette.primary.backgroundColor,
    padding: theme.palette.primary.padding,
    borderRadius: theme.palette.primary.borderRadius,
  }));
  
  // function ThemeUsage() {
  //   return (
  //     <ThemeProvider theme={customTheme}>
  //       <MyThemeComponent>Styled div with theme</MyThemeComponent>
  //     </ThemeProvider>
  //   );
  // }

  <ThemeProvider theme={customTheme}>
  <MyThemeComponent>Styled div with theme</MyThemeComponent>
</ThemeProvider>


// function EventsModel() {
        
//     return (<Typography variant="h1" component="div">Hello</Typography>);
// }

export default Events;