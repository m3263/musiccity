import React, { useEffect, useState } from 'react';
import { InputLabel, MenuItem, FormControl, Select, InputBase, Grid } from '@mui/material/';
import Paper from '@mui/material/Paper';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import SearchIcon from '@mui/icons-material/Search';
import { getEventsSearch } from '../functions/fetchEventData'
import { useHistory } from "react-router-dom";

//EVENTTODO
function EventSearchBar(props: any) {
    const { setSearch, search, setEvents, sortOrder, setCount, setSortOrder, page, pageSize, setPage, sortBy, setSortBy } = props;

    const history = useHistory();
    //history push
    useEffect(() => {
        history.push("/events?query=" + search + "&sort=" + sortBy + "&order=" + sortOrder + "&page=" + page + "&step=" + pageSize);
    }, [pageSize, page, sortBy, sortOrder, search])

    const handleSortOrder = (event: any) => {
        setPage(0)
        setSortOrder(event.target.value);
    };
    const handleSort = (event: any) => {
        setPage(0)
        setSortBy(event.target.value);
    };
    const handleSearch = (event: any) => {
        setSearch(event.target.value.toString().replace(/[^\w\s]/gi, ''));
    };

    const validate = (event: any) => {
        if (event.key === 'Enter'){
            test(event)
        }
    }
    const test = (event: any) => {
        setPage(0)
        getEventsSearch(page, pageSize, sortBy, search, sortOrder)
            .then(response => {
                setEvents(response.data['results']);
                setCount(response.data['count']);
            })
        history.push("/events?query=" + search + "&sort=" + sortBy + "&order=" + sortOrder + "&page=" + page + "&step=" + pageSize);
    }

    return (
        <div>
            <Grid container justifyContent="center" mt={6}>
                <Paper
                    component="form"
                    sx={{ p: '2px 4px', display: 'flex', alignItems: 'center', width: 800 }}
                >
                    <InputBase
                        sx={{ ml: 1, flex: 1 }}
                        placeholder="Search"
                        inputProps={{ 'aria-label': 'search' }}
                        value={search}
                        onChange={handleSearch}
                        onKeyDown={validate}
                    />
                    <IconButton onClick={test} sx={{ p: '10px' }} aria-label="search">
                        <SearchIcon />
                    </IconButton>
                    <Divider sx={{ height: 28, m: 0.5 }} orientation="vertical" />
                    <FormControl sx={{ m: 1, minWidth: 125 }} variant="standard">
                        <InputLabel id="demo-customized-select-label">Sort By</InputLabel>
                        <Select
                            labelId="demo-simple-select-error-label"
                            id="demo-simple-select-error"
                            value={sortBy}
                            onChange={handleSort}
                        >
                            <MenuItem value={"featured"}>Featured</MenuItem>
                            <MenuItem value={"venue_name"}>Venue</MenuItem>
                            <MenuItem value={"city"}>City</MenuItem>
                            <MenuItem value={"country"}>Country</MenuItem>
                            <MenuItem value={"time"}>Start Time</MenuItem>
                        </Select>
                    </FormControl>
                    <FormControl sx={{ m: 1, minWidth: 190 }} variant="standard">
                        <InputLabel id="demo-customized-select-label">Order By</InputLabel>
                        <Select
                            labelId="demo-simple-select-error-label"
                            id="demo-simple-select-error"
                            value={sortOrder}
                            onChange={handleSortOrder}
                        >
                            <MenuItem value={"desc"}>Descending</MenuItem>
                            <MenuItem value={"asc"}>Ascending</MenuItem>
                        </Select>
                    </FormControl>
                </Paper>
            </Grid>
        </div>
    );
}

export default EventSearchBar;