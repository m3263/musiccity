import React, { useEffect, useState } from 'react';
import { InputLabel, MenuItem, FormControl, Select, InputBase, Grid, OutlinedInput, ListItemText, Checkbox } from '@mui/material/';
import Paper from '@mui/material/Paper';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import SearchIcon from '@mui/icons-material/Search';
import { getArtists } from '../functions/fetchArtistData'
import { useHistory } from "react-router-dom";

const ITEM_HEIGHT = 100;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 150,
    },
  },
};

const GenreMenuProps = {
    PaperProps: {
      style: {
        maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
        width: 250,
      },
    },
  };

const genres = ['album rock', 'alt z', 'alternative hip hop', 'alternative metal', 'alternative rock', 'art pop', 'atl hip hop', 'atl trap', 'banda', 'boy band', 'brazilian hip hop', 'british invasion', 'brostep', 'canadian contemporary r&b', 'canadian pop', 'cantautor', 'celtic rock', 'classic rock', 'colombian pop', 'conscious hip hop', 'contemporary country', 'corrido', 'country', 'country road', 'country rock', 'dance pop', 'desi pop', 'dfw rap', 'double drumming', 'dutch edm', 'east coast hip hop', 'edm', 'electro', 'electro house', 'electro metropopolis', 'electronic trap', 'electropop', 'emo', 'filmi', 'florida rap', 'folk rock', 'forro', 'funk metal', 'funk rock', 'g funk', 'gangster rap', 'girl group', 'glam rock', 'grunge', 'hard rock', 'heartland rock', 'hip hop', 'hip pop', 'indietronica', 'k - pop', 'k-pop boy group', 'latin', 'latin arena pop', 'latin hip hop', 'latin pop', 'latin rock', 'latin viral pop', 'lgbtq+ hip hop', 'mariachi', 'mellow gold', 'melodic rap', 'metal', 'metropopolis', 'mexican pop', 'mexican rock', 'miami hip hop', 'modern alternative rock', 'modern bollywood', 'modern rock', 'neo mellow', 'neo soul', 'norteno', 'north carolina hip hop', 'nu metal', 'nuevo regional mexicano', 'nwobhm', 'nz pop', 'old school thrash', 'panamanian pop', 'permanent wave', 'piano rock', 'pittsburgh rap', 'pop', 'pop dance', 'pop rap', 'pop rock', 'post - grunge', 'post-teen pop', 'progressive electro house', 'progressive house', 'puerto rican pop', 'punjabi pop', 'punk', 'queens hip hop', 'r & b', 'ranchera', 'rap', 'rap df', 'rap latina', 'rap metal', 'reggaeton', 'reggaeton colombiano', 'reggaeton flow', 'regional mexican', 'rock', 'rock en espanol', 'scandipop', 'sertanejo', 'sertanejo pop', 'sertanejo universitario', 'slap house', 'soft rock', 'south carolina hip hop', 'southern hip hop', 'swedish electropop', 'swedish pop', 'talent show', 'thrash metal', 'trap', 'trap boricua', 'trap brasileiro', 'trap latino', 'tropical house', 'uk pop', 'urban contemporary', 'viral pop', 'viral rap', 'west coast rap', 'yacht rock']
const alpha = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']

function ArtistsSelection(props: any) {
    const { related, setRelated, genre, setGenre, setSearch, search, setArtists, sortOrder, setSortOrder, itemsPerPage, page, setCount, setPage, sort, setSort } = props;
    
    const history = useHistory();

    useEffect(() => {
        history.push("/artists?query=" + search + "&sort=" + sort+ "&order=" + sortOrder + "&page=" + page + "&step=" + itemsPerPage + "&genre=" + genre.join(',') + "&related=" + related.join(','));
    }, [itemsPerPage, page, genre, related, sort, sortOrder])

    const handleChange = (event) => {
        const {
          target: { value },
        } = event;
        setGenre(
          typeof value === 'string' ? value.split(',') : value,
        );
      };

      const handleChangeRelated = (event) => {
        const {
          target: { value },
        } = event;
        setRelated(
          typeof value === 'string' ? value.split(',') : value,
        );
      };

    const handleSortOrder = (event: any) => {
        setPage(0)
        setSortOrder(event.target.value);
    };

    const handleSort = (event: any) => {
        setPage(0)
        setSort(event.target.value);
    };
    const handleSearch = (event: any) => {
        setSearch(event.target.value.toString().replace(/[^\w\s]/gi, ''));
    };
    const validate = (event: any) => {
        if (event.key === 'Enter'){
            test(event)
        }
    }

    const test = (event: any) => {
        setPage(0)
        getArtists(search, itemsPerPage, page, sort, sortOrder, genre, related)
        .then(response => {
            setArtists(response['results']);
            setCount(response['count']);
        })
        history.push("/artists?query=" + search + "&sort=" + sort+ "&order=" + sortOrder + "&page=" + page + "&step=" + itemsPerPage + "&genre=" + genre.join(',') + "&related=" + related.join(','));
    }

    return (
    <div>
        <Grid container justifyContent="center" mt={6}>
            <Paper
            component="form"
            sx={{ p: '2px 4px', display: 'flex', alignItems: 'center', width: 1000 }}
            >
            <InputBase
                sx={{ ml: 1, flex: 1 }}
                placeholder="Search"
                inputProps={{ 'aria-label': 'search' }}
                value={search}
                onChange={handleSearch}
                onKeyDown={validate}
            />
            <IconButton onClick={test} sx={{ p: '10px' }} aria-label="search">
                <SearchIcon />
            </IconButton>
            <Divider sx={{ height: 35, m: 0.5 }} orientation="vertical" />
            <FormControl sx={{m: 1, width: 180 }} variant="standard">
                <InputLabel id="demo-customized-select-label">Combined Genre</InputLabel>
                <Select
                labelId="demo-multiple-checkbox-label"
                id="demo-multiple-checkbox"
                multiple
                value={genre}
                onChange={handleChange}
                renderValue={(selected) => selected.join(', ')}
                MenuProps={GenreMenuProps}
                >
                    {genres.map((name) => (
                        <MenuItem key={name} value={name}>
                        <Checkbox checked={genre.indexOf(name) > -1} />
                        <ListItemText primary={name} />
                        </MenuItem>
                    ))}
                </Select>
            </FormControl>
            <FormControl sx={{m: 1, width: 150 }} variant="standard">
                <InputLabel id="demo-customized-select-label">Related Artists</InputLabel>
                <Select
                labelId="demo-multiple-checkbox-label"
                id="demo-multiple-checkbox"
                multiple
                value={related}
                onChange={handleChangeRelated}
                renderValue={(selected) => selected.join(', ')}
                MenuProps={MenuProps}
                >
                    {alpha.map((name) => (
                        <MenuItem key={name} value={name}>
                        <Checkbox checked={related.indexOf(name) > -1} />
                        <ListItemText primary={name} />
                        </MenuItem>
                    ))}
                </Select>
            </FormControl>
            <FormControl sx={{ m: 1, minWidth: 120 }} variant="standard">
                <InputLabel id="demo-customized-select-label">Sort By</InputLabel>
                <Select
                labelId="demo-simple-select-error-label"
                id="demo-simple-select-error"
                value={sort}
                onChange={handleSort}
                >
                <MenuItem value="name">
                    <em>Name</em>
                </MenuItem>
                <MenuItem value={"popularity"}>Popularity</MenuItem>
                <MenuItem value={"followers"}>Followers</MenuItem>
                </Select>
            </FormControl>
            <FormControl sx={{ m: 1, minWidth: 120 }} variant="standard">
                <InputLabel id="demo-customized-select-label">Sort Order</InputLabel>
                <Select
                labelId="demo-simple-select-error-label"
                id="demo-simple-select-error"
                value={sortOrder}
                onChange={handleSortOrder}
                >
                <MenuItem value="asc">
                    <em>Ascending</em>
                </MenuItem>
                <MenuItem value={"desc"}>Descending</MenuItem>
                </Select>
            </FormControl>
        </Paper>
    </Grid>
    </div>
  );
}

export default ArtistsSelection;