import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { Grid, Box, CircularProgress, Typography, Paper, Table, TableCell, TableRow, TableBody, TableContainer, TableHead } from '@mui/material/';
// import { getRelatedArtists } from '../functions/fetchArtistData'

function RelatedArtists(props) {
    const { data } = props;
    const [relatedArtists, setRelatedArtists] = useState([]);

    useEffect(() => {
        if (Object.keys(data).length !== 0) {
            setRelatedArtists(data['related'])
        }     
    }, [data]);


    if (relatedArtists.length === 0){
        return (
            <></>
        );
    }

    return (
        <>
            <Box  sx={{ width: '90%', overflow: 'hidden' }} ml={5}>
                <Typography variant="h4" component="div" mb={5} ml={4}>
                    Related Artists:
                </Typography>
            </Box>
            <Grid container justifyContent="center">
                <Paper sx={{ width: '80%', overflow: 'hidden' }}>
                <TableContainer  sx={{ maxHeight: 450 }}>
                    <Table stickyHeader aria-label="sticky table">
                    <TableHead>
                        <TableRow>
                            <TableCell>
                                Artist
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {relatedArtists.map((row: any) => {
                            return (
                                // <TableRow hover role="checkbox" tabIndex={-1} key={row.id} component={Link} to={"/artists/" + row.id} style={{ textDecoration: 'none' }}>
                                <TableRow hover role="checkbox" tabIndex={-1} key={row.id}>
                                    {/* <TableCell>
                                        <img src={row.images[0].url}  width="55" height="55"  alt={row.name} />
                                    </TableCell> */}
                                    <TableCell>
                                        {row}
                                    </TableCell>
                                    {/* <TableCell>
                                        {row.genres[0]}
                                    </TableCell>
                                    <TableCell>
                                        {row.followers.total}
                                    </TableCell> */}
                                </TableRow>
                            );
                        })}
                    </TableBody>
                    </Table>
                </TableContainer>
            </Paper>
            </Grid>
        </>
    );
}

export default RelatedArtists;