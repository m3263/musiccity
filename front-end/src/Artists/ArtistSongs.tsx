import React, { useState, useEffect } from 'react';
import { styled } from '@mui/material/styles';
import ArrowForwardIosSharpIcon from '@mui/icons-material/ArrowForwardIosSharp';
import MuiAccordion, { AccordionProps } from '@mui/material/Accordion';
import MuiAccordionSummary, { AccordionSummaryProps } from '@mui/material/AccordionSummary';
import MuiAccordionDetails from '@mui/material/AccordionDetails';
import { Typography, Grid, Box } from '@mui/material/';
import ReactPlayer from 'react-player/soundcloud'
import ReactHtmlParser from 'react-html-parser'; 


const Accordion = styled((props: AccordionProps) => (
    <MuiAccordion disableGutters elevation={0} square {...props} />
  ))(({ theme }) => ({
    border: `1px solid ${theme.palette.divider}`,
    '&:not(:last-child)': {
      borderBottom: 0,
    },
    '&:before': {
      display: 'none',
    },
  }));
  
  const AccordionSummary = styled((props: AccordionSummaryProps) => (
    <MuiAccordionSummary
      expandIcon={<ArrowForwardIosSharpIcon sx={{ fontSize: '0.9rem' }} />}
      {...props}
    />
  ))(({ theme }) => ({
    backgroundColor:
      theme.palette.mode === 'dark'
        ? 'rgba(255, 255, 255, .05)'
        : 'rgba(0, 0, 0, .03)',
    flexDirection: 'row-reverse',
    '& .MuiAccordionSummary-expandIconWrapper.Mui-expanded': {
      transform: 'rotate(90deg)',
    },
    '& .MuiAccordionSummary-content': {
      marginLeft: theme.spacing(1),
    },
  }));
  
  const AccordionDetails = styled(MuiAccordionDetails)(({ theme }) => ({
    padding: theme.spacing(2),
    borderTop: '1px solid rgba(0, 0, 0, .125)',
  }));

function ArtistSongs(props) {
    const { songData, expanded, handleChange } = props;
    const [soundCloudURL, setSoundCloudURL] = useState("")
    const [YTURL, setYTURL] = useState("")
    const [lyrics, setLyrics] = useState("")

    useEffect(() => {
        for (var i = 0; i < songData['song']['media'].length; i++) {
            if (songData['song']['media'][i]['provider'] === "soundcloud") {
                setSoundCloudURL(songData['song']['media'][i]['url'])
            }
            if (songData['song']['media'][i]['provider'] === "youtube") {
                var url = songData['song']['media'][i]['url']
                url = [url.slice(0, 4), 's', url.slice(4)].join('');
                url = [url.slice(0, 24), 'embed/', url.slice(32)].join('');
                setYTURL(url)
            }
        }
        setLyrics(songData['song']['embed_content'])
    }, [songData]);

    return (
            <Accordion expanded={expanded === songData['song']['title']} onChange={handleChange(songData['song']['title'])}>
                <AccordionSummary aria-controls="panel1d-content" id="panel1d-header">
                  <Typography>{songData['song']['title']}</Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <Grid container justifyContent="center" alignItems="center">
                        <Box mt={5}>
                            <div>
                                {soundCloudURL !== "" ? <ReactPlayer height="30" url={soundCloudURL} show_artwork={true}/> : <></>}
                            </div>
                        </Box>
                            <Box>
                                {YTURL !== "" ?
                                    <iframe
                                    width="800"
                                    height="480"
                                    src={YTURL}
                                    frameBorder="0"
                                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                    allowFullScreen
                                    title="Embedded youtube"
                                    /> : <></>
                                }
                            </Box>
                        <div> { ReactHtmlParser (lyrics)} </div>
                    </Grid>
                </AccordionDetails>
            </Accordion>
    );
}

export default ArtistSongs;