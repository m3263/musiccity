import React from 'react';
import { Link } from 'react-router-dom';
import { Box, Grid, Paper, Typography, Divider } from '@mui/material/';
import { makeStyles, Button } from "@material-ui/core";
import { formatTime } from "../functions/formatting"

const useStyles = makeStyles((theme) => ({
	link: {
	  color: "green",
	  fontSize: "18px",
	  textTransform: 'none',
	},
  }));

function ArtistEvents(props) {
    const { relationships } = props;
    const classes = useStyles();
    
    if (Object.keys(relationships).length === 0) {
        return (<></>);
    }

    return (
        <Paper>
            {relationships.map((data: any) => (
                <Grid container>
                    <Grid item xs={9}>
                        <Link to={"/events/" + data.event.id} style={{ textDecoration: 'none' }}>
                            <Box sx={{ height: 80 }}>
                                <Typography variant="h5" component="div">
                                    {data.event.venue_name}
                                </Typography>
                                <Typography variant="subtitle1" component="div">
                                    {formatTime(data.event.date)}
                                </Typography>
                                <Typography variant="subtitle2" component="div">
                                    {data.event.city}, {data.event.country}
                                </Typography>
                            </Box>
                        </Link>
                        <Divider flexItem />
                    </Grid>
                    <Grid item xs={3} style={{textAlign: "center"}}>
                        <Box display='flex' textAlign='center' sx={{ height: 80, alignItems: "center", }}>
                            <Link to={"/cities/" + data.city.id} style={{ textDecoration: 'none' }}>
                                <Button className={classes.link}>
                                More Events in this City
                                </Button>
                            </Link>
                        </Box>
                        <Divider flexItem />
                    </Grid>
                </Grid>
            ))}
        </Paper>
    );
}

export default ArtistEvents;