import React from 'react';
import { Link } from 'react-router-dom';
import { Grid, Card, Box, Typography, CardActionArea } from '@mui/material/';
import LinearProgress from '@mui/material/LinearProgress';
import ArtistCard from './ArtistCard'

function ArtistsView(props: any) {
    const { artists, search, genre, related } = props;

    return (
        <Box sx={{ flexGrow: 1 }}>
            <Grid container alignItems="center" justifyContent="center" mt={4}>
                <Grid container rowSpacing={3} columnSpacing={3} alignItems="center" justifyContent="center" sx={{ maxWidth: "90%" }}>
                    {artists.map((elem: any) => (<ArtistCard data={elem} search={search} genre={genre} related={related}/>
                    ))}
                </Grid>
            </Grid>
        </Box>
    );
}

export default ArtistsView;