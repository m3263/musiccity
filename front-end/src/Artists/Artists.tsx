import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { Grid, Box, Typography, CircularProgress } from '@mui/material/';
import ArtistsSelection from './ArtistsSelection';
import CustomPagination from '../shared_components/CustomPagination';
import ArtistsView from './ArtistsView';
import { getArtists } from '../functions/fetchArtistData'


function Artists({location}) {
    let params = new URLSearchParams(location.search);
    const [artists, setArtists] =  useState([]);
    const [sort, setSort] = useState(params.get("sort") === null ? 'name' : params.get("sort")); 
    const [sortOrder, setSortOrder] = useState(params.get("order") === null ? "asc" : params.get("order")); 
    const [page, setPage] = useState(params.get("page") === null ? 0 : parseInt(params.get("page"))); 
    const [itemsPerPage, setItemsPerPage] = useState(params.get("step") === null ? 10 : parseInt(params.get("step"))); 
    const [count, setCount] = useState(10); 
    const [loading, setLoading] = useState(false); 
    const [search, setSearch] = useState(params.get("query") === null ? "" : params.get("query"));
    const [genre, setGenre] = React.useState(params.get("genre") === null || params.get("genre") === "" ? [] : params.get("genre").split(','));
    const [related, setRelated] = React.useState(params.get("related") === null || params.get("related") === "" ? [] : params.get("related").split(','));

    useEffect(() => {
        setLoading(true)
        getArtists(search, itemsPerPage, page, sort, sortOrder, genre, related)
        .then(response => {
            setArtists(response['results']);
            setCount(response['count']);
            setLoading(false)
        })
    }, [page, itemsPerPage, sort, sortOrder, genre, related]);

    return (
        <div>
            <Grid container alignItems="center" justifyContent="center">
                <Grid item xs={12}>
                    <ArtistsSelection 
                        search={search} 
                        setSearch={setSearch} 
                        setArtists={setArtists} 
                        sortOrder={sortOrder} 
                        setSortOrder={setSortOrder} 
                        itemsPerPage={itemsPerPage} 
                        page={page} 
                        setCount={setCount} 
                        setPage={setPage} 
                        sort={sort} 
                        setSort={setSort}
                        genre={genre}
                        setGenre={setGenre}
                        related={related}
                        setRelated={setRelated}
                    />
                </Grid>
                {loading ? (
                <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center'}} mt={10}>
                    <CircularProgress />
                </Box>) :
                (
                <div>
                    <Grid item xs={12}>
                        <ArtistsView artists={artists} search={search} genre={genre} related={related}/>
                    </Grid>
                    <Grid item xs={12}>
                        <Grid container alignItems="center" justifyContent="center">
                            <CustomPagination page={page} setPage={setPage} itemsPerPage={itemsPerPage} setItemsPerPage={setItemsPerPage} count={count}/>
                        </Grid>
                    </Grid>
                </div>
                )}
                
            </Grid>
            
        </div>
    );
}

export default Artists;