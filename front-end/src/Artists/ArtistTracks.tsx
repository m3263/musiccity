import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { Grid, Box, CircularProgress, Typography, Paper, Table, TableCell, TableRow, TableBody, TableContainer, TableHead } from '@mui/material/';
import { formatDate } from '../functions/formatting'

function ArtistTracks(props) {
    const { data } = props;
    const [tracks, setTracks] = useState([]);

    useEffect(() => {
        if (Object.keys(data).length !== 0) {
            setTracks(data['tracks'])
        }     
    }, [data]);

    if (tracks.length === 0) {
        return (
            <></>
        );
    }

    // // useEffect(() => {
    // //     getArtistTracks(id)
    // //     .then(response => {
    // //         setTracks(response)
    // //     })
    // // }, [id]);

    // if (tracks.length === 0){
    //     return (
    //         <></>
    //     );
    // }

    return (
        <>
          <Grid container rowSpacing={3} columnSpacing={5} mt={3} mb={10} justifyContent="center"> 
                <Paper sx={{ width: '75%', overflow: 'hidden' }}>
                    <Typography variant="h4" component="div" align="center" mb={5}>
                        Top Tracks:
                    </Typography>
                    <TableContainer sx={{ maxHeight: 440 }}>
                        <Table stickyHeader aria-label="sticky table">
                        <TableBody>
                            {tracks.map((row: any) => {
                                var row = JSON.parse(row)
                                return (
                                    <TableRow hover role="checkbox" tabIndex={-1} key={row.code}>
                                        <TableCell>
                                            <img src={row.album.images[0].url}  width="55" height="55"  alt={row.name} />
                                        </TableCell>
                                        <TableCell>
                                            {row.name}
                                        </TableCell>
                                        <TableCell>
                                            {row.album.name}
                                        </TableCell>
                                        <TableCell>
                                            {formatDate(row.album.release_date)}
                                        </TableCell>
                                        <TableCell>
                                            {row.preview_url === null ? 'No preview' : <audio controls src={row.preview_url}></audio> }
                                        </TableCell>
                                    </TableRow>
                                );
                            })}
                        </TableBody>
                        </Table>
                    </TableContainer>
                </Paper>
            </Grid>
        </>
    );
}

export default ArtistTracks;