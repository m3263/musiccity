import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Grid, Card, Box, Typography, CardActionArea, CardActions } from '@mui/material/';
import LinearProgress from '@mui/material/LinearProgress';
import { formatNumber, formatStr } from '../functions/formatting'

var Highlight = require('react-highlighter');

function contains(str, search) {
    if (search === "") {
        return ""
    }
    search = search.split(" ")
    for (var j = 0; j < search.length; j++) {
        for (var i = 0; i < str.length; i++) {
            if (str[i].toLowerCase().includes(search[j].toLowerCase())) {
                return str[i]
            }
        }
    }
    return ""
}

function startsWith(str, search) {
    var result = ""
    for (var j = 0; j < search.length; j++){
        for (var i = 0; i < str.length; i++) {
            if (str[i].toLowerCase().startsWith(search[j].toLowerCase())) {
                result += str[i] + ", "
            }
        }
    }
    if (result === "") {
        return result
    }
    return result.slice(0, result.length - 2)
}

function ArtistCard(props) {
    const { data, search, genre, related } = props;

    if (data.genres === undefined) {
        return (
        <Grid item key={data.key}>
            <CardActionArea>
                <Link to={data.path} style={{textDecoration: 'none'}}>
                    <Card key={data.key} style={{ width: 220, height: 305 }}>
                        <Grid container alignItems="center" justifyContent="center" mt={15}>
                            <Grid item>
                                <Typography variant="h6" component="div">
                                    {data.name}
                                </Typography>
                            </Grid>
                            <Grid item>
                                <Typography variant="subtitle2" component="div">
                                    {data.count} Results
                                </Typography>
                            </Grid>
                        </Grid>
                    </Card>
                </Link>
            </CardActionArea>
        </Grid>
        );
    }

    var genreItem = contains(data.genres, search)
    if (genreItem === "" && genre.length > 0) {
        genreItem = genre.join(", ")
    }

    var relatedItem = contains(data.related, search)
    if (relatedItem === "" && related.length > 0) {
        relatedItem = startsWith(data.related, related)
    }

    const pattern = new RegExp(search.trim().replace(" ", "|").replace("$", "\\$"), "i")

    return (
        <Grid item key={data.key}>
            <CardActionArea component={Link} to={{pathname: "/artists/" + data.id}}>
                <Card key={data.key} style={{ width: 220, height: 305 }}>
                    <img src={data.image}  width="220" height="200"  alt={data.name}/>
                    <Box sx={{ height: 30 }} ml={1}>
                        <Highlight style={{ fontSize: '22px' }} search={search === "" ? "" : pattern}>{formatStr(data.name, 28)}</Highlight>
                    </Box>
                    <Box sx={{ height: 17 }} ml={1}>
                        <Highlight style={{ fontSize: '15px' }} search={search === "" ? "" : pattern}>{genreItem === "" ? data.genres[0] : formatStr(genreItem, 31)}</Highlight>
                    </Box>
                    <Box sx={{ height: 17 }} ml={1}>
                        <Highlight style={{ fontSize: '15px' }} search={search === "" ? "" : pattern}>{formatNumber((data.followers).toString())}</Highlight> Spotify Followers
                    </Box>
                    <Box sx={{ height: 17 }} ml={1}>
                    Similar to <Highlight style={{ fontSize: '15px' }} search={search === "" ? "" : pattern}>{relatedItem === "" ?  formatStr(data.related[0], 19) : formatStr(relatedItem, 19)}</Highlight>
                    </Box>
                    <Box sx={{ height: 17 }} ml={1}>
                        Popularity: <Highlight style={{ fontSize: '15px' }} search={search === "" ? "" : pattern}>{(data.popularity/10).toString()}</Highlight>
                    </Box>
                    <LinearProgress variant="determinate" value={data.popularity} />
                </Card>
            </CardActionArea>
        </Grid>
    );
}

export default ArtistCard;