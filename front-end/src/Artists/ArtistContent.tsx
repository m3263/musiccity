import React from 'react';
import { Link } from 'react-router-dom';
import { Grid, Box, Typography, Card, Paper } from '@mui/material/';
import { formatNumber } from '../functions/formatting'


function ArtistContent(props: any) {
    const { data } = props
    var image = "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/480px-No_image_available.svg.png"

    if (Object.keys(data).length === 0) {
        return(<></>);
    }

    return (
        <div>
            <Box sx={{ flexGrow: 1 }} mt={7}>
                <Grid container alignItems="center" ml={7}> 
                    <Paper sx={{ width: "100%" }}>
                        <Box>
                            <img src={data.image}  width="300" height="300"  alt={data.name} />          
                            <Typography variant="h3" component="div">
                                {data.name}
                            </Typography>
                        </Box>
                        <Grid container rowSpacing={3} columnSpacing={4} alignItems="center" mt={1} mb={4} mr={4}> 
                            {data.genres.map((elem: any) => (
                                <Grid item key={elem}>
                                    <Card>
                                        <Typography variant="h6" component="div">
                                            {elem}
                                        </Typography>
                                    </Card>
                                </Grid>
                            ))}
                        </Grid>
                        <Grid container rowSpacing={3}> 
                            <Grid item xs={12}>
                                <Typography variant="h6" component="div">
                                    {formatNumber(data.followers)} Spotify Followers
                                </Typography>
                            </Grid>
                        </Grid>
                    </Paper>
                </Grid>
            </Box>
        </div>
    );
}

export default ArtistContent;