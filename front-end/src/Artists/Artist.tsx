import React, { useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import { Grid, Box,CircularProgress, Typography} from '@mui/material/';
import ArtistContent from './ArtistContent';
import { getArtistData, getArtistRelationships } from '../functions/fetchArtistData'
import RelatedArtists from './RelatedArtists'
import ArtistAlbums from './ArtistAlbums'
import ArtistTracks from './ArtistTracks';
import ArtistSongs from './ArtistSongs';
import ArtistEvents from './ArtistEvents';

function Artist(props: any) {
    const params: any = useParams();
    // const query = new URLSearchParams(props.location.search);
    // const data = require("../data/artists_data.json")[String(params.id)]
    const [artistData, setArtistData] = useState({});
    const [songData, setSongData] = useState([]);
    const [expanded, setExpanded] = useState<string | false>('panel1');
    const [relationships, setRelationships] = useState({});

    const handleChange =
    (panel: string) => (event: React.SyntheticEvent, newExpanded: boolean) => {
      setExpanded(newExpanded ? panel : false);
    };

    useEffect(() => {
        getArtistRelationships(params.id)
        .then(response => {
            setRelationships(response)
        })
    }, []);

    useEffect(() => {
        getArtistData(params.id)
        .then(response => {
            setArtistData(response)
            setSongData(response['songs'])
        })
    }, []);

    if (Object.keys(artistData).length === 0) {
        return (
            <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center'}} mt={20}>
                <CircularProgress />
            </Box>
        );
    }

    return (
        <div>
            <Grid container justifyContent="center">
                <Grid item xs={12} lg={6}>
                    <ArtistContent data={artistData}/>
                </Grid>
                <Grid item xs={12} lg={6} mt={7}>
                    <RelatedArtists data={artistData}/> 
                </Grid>
                <Grid item xs={12} mt={4}>
                    <ArtistAlbums data={artistData}/>
                </Grid>
                <Grid item xs={12} justifyContent="center">
                    <ArtistTracks data={artistData}/>
                </Grid>
                <Grid item xs={12} justifyContent="center">
                    <Grid container justifyContent="center"> 
                        <Grid container rowSpacing={3} columnSpacing={5} mb={10} justifyContent="center" sx={{ maxWidth: "80%" }}> 
                            <Grid item xs={12} mb={5}>
                                <Typography variant="h4" component="div">
                                    Songs
                                </Typography>
                            </Grid>
                            <Grid item xs={12} mb={5}>
                                {songData.map((elem: any) => (
                                    <ArtistSongs songData={JSON.parse(elem)} expanded={expanded} handleChange={handleChange}/>
                                ))}
                            </Grid>
                            <Grid item xs={12} mb={5}>
                                <Typography variant="h4" component="div">
                                    Upcoming events
                                </Typography>
                            </Grid>
                            <Grid item xs={12} mb={5}>
                                <ArtistEvents relationships={relationships}/>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </div>
    );
}

export default Artist;
