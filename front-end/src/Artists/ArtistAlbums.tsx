import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { Grid, Paper, Typography, Card, CardActionArea } from '@mui/material/';
import { makeStyles } from "@material-ui/core";
import { formatDate } from '../functions/formatting'

const useStyles = makeStyles((theme) => ({
    card: {
        width: "150px",
        // height: "220px"
    }
}));

function ArtistAlbums(props) {
    const { data } = props;
    const [albums, setAlbums] = useState([]);
    const classes = useStyles();

    // console.log(albums)

    useEffect(() => {
        if (Object.keys(data).length !== 0) {
            var albums = data['albums']
            var names = []
            var trimmed = []
            for (var i = 0; i < albums.length; i++) {
                if (!names.includes( JSON.parse(albums[i])['name'])) {
                    names.push( JSON.parse(albums[i])['name'])
                    trimmed.push(albums[i])
                }
            }
            setAlbums(trimmed)
        }     
    }, [data]);

    if (albums.length === 0) {
        return (
            <></>
        );
    }

    return (
            <Grid container rowSpacing={3} columnSpacing={5} mt={3} mb={10} justifyContent="center"> 
                <Grid container rowSpacing={3} columnSpacing={5} mt={3} mb={10} justifyContent="center" alignItems="center" sx={{ maxWidth: "70%" }}> 
                    <Grid item xs={12}>
                        <Grid container justifyContent="center"> 
                            <Typography variant="h4" component="div">
                                Albums:
                            </Typography>
                        </Grid>
                    </Grid>
                    {albums.sort((a: any, b: any) => (a.release_date > b.release_date ? -1 : 1)).map((elem: any) => {
                        var data = JSON.parse(elem)

                        return (
                        <Grid item>
                            <CardActionArea href={data.link} target="_blank">
                                <Card key={data.link} className={classes.card}>
                                    <img src={data.image} width="150" height="150" alt={data.name}/>
                                    <Typography variant="subtitle2" component="div" mt={0.5} ml={0.5}>
                                        {data.name}
                                    </Typography>
                                    <Typography variant="subtitle2" component="div" ml={0.5}>
                                        {formatDate(data.date)}
                                    </Typography>
                                </Card>
                            </CardActionArea>
                        </Grid>
                    );
                    })}
                 </Grid>
            </Grid>
    );
}

export default ArtistAlbums;