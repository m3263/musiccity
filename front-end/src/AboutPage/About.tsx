import React, { useEffect, useState } from 'react';
import GitlabData from './GitlabData';
import { Grid, Box, Typography, Paper } from '@mui/material/';
import GenericCard from '../shared_components/GenericCard'

interface issueData {
    authored: any,
    closed: any,
    open: any
}

const issueStats = async(id) => {
    let x: issueData = {authored: "", closed: "", open: ""}
    let url: string = 'https://gitlab.com/api/v4/projects/29860271/issues_statistics';
    if (id !== '') {
        url += '?author_username=' + id
    }
    await fetch(url)
    .then(response => response.json())
    .then(response => {
        response = response["statistics"]["counts"]
        x.authored = response['all']
        x.closed = response['closed']
        x.open = response['opened']
    })
    return x;
}


let x: issueData = {authored: "", closed: "", open: ""}

function About() { 
    const [numCommits, setNumCommits] = useState("");
    const [gitlabCommitData, setGitlabCommitData] = useState({"anshulmodh": "", "ThoCoTho": "", "annathomas2384": "", "yk0610": "", "anirudhgoyal":""});
    const [issueData, setIssueData] = useState({"anshulmodh": x, "ThoCoTho": x, "annathomas2384": x, "yk0610": x, "anirudhgoyal": x, "main": x});
    const data = require("../data/gitlab.json").gitlab;
    const tools = require("../data/tools.json").tools;
    const apis = require("../data/apis.json").apis;

    
    useEffect(() => {
        const fetchData = async () => {
            let issueData = {"anshulmodh": await issueStats("anshulmodh"), "ThoCoTho": await issueStats("ThoCoTho"), "annathomas2384": await issueStats("annathomas2384"), "yk0610": await issueStats("yk0610"), "anirudhgoyal":await issueStats("anirudhgoyal"), "main": await issueStats("")}
            setIssueData(issueData);
        }
        fetchData()
    }, []);

    useEffect(() => {
        fetch('https://gitlab.com/api/v4/projects/29860271/repository/contributors')
        .then(response => response.json())
        .then(response => {
            let mapping: any = {"thocotho": "ThoCoTho", "anshulmodh": "anshulmodh", "Thomas Connor Thompson": "ThoCoTho", "Anna Thomas": "annathomas2384", "anirudh-goyal": "anirudhgoyal", "yk0610": "yk0610"}
            let commits: any = {"anshulmodh": 0, "ThoCoTho": 0, "annathomas2384": 0, "yk0610": 0, "anirudhgoyal": 0};
            let total_commits = 0
            for(let i = 0; i < response.length; i++) {
                let data = response[i];
                let author = data.name;
                if (author in mapping) {
                    commits[mapping[author]] += data['commits'];
                }
                total_commits += data['commits'];
            }
            setGitlabCommitData(commits);
            setNumCommits(String(total_commits));
        })
    }, []);

    return (
        <div>
            <Grid container rowSpacing={5} alignItems="center" justifyContent="center" p={10}>
                <Grid item xs={12}>
                    <Typography variant="h3" component="div">About Us</Typography>
                </Grid>
                <Grid item xs={12}>
                    <Paper>
                        <Typography variant="h6" component="div">
                            Music City helps people establish 
                            roots in their communities and engage with the artistic environment 
                            of the city, all while supporting local artists. We do this by offering
                            information about all kinds of artists, live music events, and cities
                            in which to see those events. 
                            
                        </Typography>
                    {/* </Grid>
                    <Grid item xs={12}> */}
                        <Typography variant="h6" component="div" mt={3}>
                            By integrating the data from the three models on our website, we 
                            construct a full picture of the music scene in cities across the 
                            country, which allows us to track which cities are hotbeds for live 
                            music and which cities have a more sparse distribution of concerts 
                            and festivals. It also helps us track which cities popular artists 
                            frequent and which cities popular artists overlook.
                        </Typography>
                    </Paper>
                </Grid>
                <Grid item xs={12}>
                    <Typography variant="h3" component="div">Our Team</Typography>
                </Grid>
                <Grid item xs={12}>
                    <GitlabData data={data} 
                        gitlabCommitData={gitlabCommitData}
                        issueData={issueData}
                        numCommits={numCommits}
                    />
                </Grid>
                <Grid item xs={12}>
                    <Typography variant="h3" component="div">Tools Used</Typography>
                </Grid>
                <Grid item xs={12}>
                    <Box sx={{ flexGrow: 1 }}>
                        <Grid container rowSpacing={10} columnSpacing={10} alignItems="center" justifyContent="center">
                            {tools.map((elem: any) => (
                                <Grid item style={{textAlign: "center"}} key={elem.key}>
                                    <GenericCard image={elem.image} title={elem.name} subtitle={elem.subtitle} link={elem.link}/>
                                </Grid>
                            ))}
                        </Grid>
                    </Box>
                </Grid>
                <Grid item xs={12}>
                    <Typography variant="h3" component="div">APIs Used</Typography>
                </Grid>
                <Grid item xs={12}>
                    <Box sx={{ flexGrow: 1 }} mt={5}>
                        <Grid container rowSpacing={10} columnSpacing={10} alignItems="center" justifyContent="center">
                            {apis.map((elem: any) => (
                                <Grid item style={{textAlign: "center"}} key={elem.key}>
                                    <GenericCard image={elem.image} title={elem.name} subtitle={elem.subtitle} link={elem.link}/>
                                </Grid>
                            ))}
                        </Grid>
                    </Box>
                </Grid>
                <Grid item xs={12}>
                    <Box sx={{ flexGrow: 1 }} mt={5}>
                        <Typography variant="h3" component="div">Our Application</Typography>
                    </Box>
                </Grid>
                <Grid item xs={12}>
                    <Box sx={{ flexGrow: 1 }} mt={5}>
                        <Grid container rowSpacing={10} columnSpacing={10} alignItems="center" justifyContent="center">
                            <Grid item style={{textAlign: "center"}}>
                                <GenericCard image={"/images/gitlab.png"} title={"Code Repository"} subtitle={""} link={"https://gitlab.com/m3263/musiccity/"}/>
                            </Grid>
                            <Grid item style={{textAlign: "center"}}>
                                <GenericCard image={"/images/Postman.png"} title={"API Documentation"} subtitle={""} link={"https://documenter.getpostman.com/view/4757637/UUy4ckYz"}/>
                            </Grid>
                        </Grid>
                    </Box>
                </Grid>
            </Grid>
        </div>

    );
  }
  
export default About;