import React from 'react';
import GitlabStatistics from './GitlabStatistics';
import { CardMedia, Card, CardContent, Typography, Box, CardActionArea } from '@mui/material/';

function Person(props: any) {
    const { user, commitData, issueData, name, profile, bio, extra, linkedin, tests } = props;

    return (
        <CardActionArea href={linkedin}>
            <Card sx={{ width: 360, height: 690 }}>
                <CardMedia
                    component="img"
                    height="400"
                    image={profile}
                    alt={name}
                />
                <CardContent>
                    <Box height="160px">
                        <Typography variant="h5" component="div">
                            {name}
                        </Typography>
                        <Typography variant="subtitle2" component="div">
                            Full-Stack
                        </Typography>
                        <Typography variant="subtitle2" component="div">
                            {extra}
                        </Typography>
                        <Typography variant="subtitle2" component="div">
                            {bio}
                        </Typography>
                    </Box>
                    <Box mt={2}>
                        <GitlabStatistics numCommits={commitData[user]} numOpenIssues={issueData[user].open} numClosedIssues={issueData[user].closed} numAuthoredIssues={issueData[user].authored} numTests={tests}/>
                    </Box>
                </CardContent>
            </Card>
        </CardActionArea>
    );
}

export default Person;
