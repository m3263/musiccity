import Person from './Person'
import { Grid, Box, CircularProgress, Card, CardContent, Typography } from '@mui/material/';
import { ReactComponent as CodeIcon } from './icons/code.svg'
import GitlabStatistics from './GitlabStatistics';

function isEmpty(numCommits: String, issueData: any) {
    return numCommits === "" || issueData.open === "" || issueData.closed === "" || issueData.authored === ""; 
}

function GitlabData(props: any) {
    const { data, gitlabCommitData, issueData, numCommits } = props;
    if (isEmpty(numCommits, issueData.main)) {
        return (
            <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
              <CircularProgress />
            </Box>
          );
    }

    return (
        <Grid container rowSpacing={10} alignItems="center" justifyContent="center">
            <Grid item xs={12}>
                <Grid container rowSpacing={10} columnSpacing={10} alignItems="center" justifyContent="center">
                    {data.map((elem: any) => (
                        <Grid item style={{textAlign: "center"}} key={elem.key}>
                            <Person user={elem.user} name={elem.name} commitData={gitlabCommitData} issueData={issueData} key={elem.key} profile={elem.profile} bio={elem.bio} extra={elem.extra} linkedin={elem.linkedin} tests={elem.tests}/>
                        </Grid>
                    ))}
                </Grid>
            </Grid>
            <Grid item xs={12}>
                <Box sx={{ flexGrow: 1 }}>
                    <Grid container rowSpacing={10} columnSpacing={30} alignItems="center" justifyContent="center">
                        <Grid item style={{textAlign: "center"}}>
                            <Card sx={{ width: 600 }}>
                                <CodeIcon fill={"#A9A9A9"} style={{ height: 12 * 10, width: 12 * 10 }} />
                                <CardContent>
                                    <Typography variant="h4" component="div">
                                        Main Branch Statistics
                                    </Typography>
                                    <GitlabStatistics numCommits={numCommits} numOpenIssues={issueData.main.open} numClosedIssues={issueData.main.closed} numAuthoredIssues={ issueData.main.authored} numTests={40}/>
                                </CardContent>
                            </Card>
                        </Grid>
                    </Grid>
                </Box>
            </Grid>
        </Grid>
    );
}

export default GitlabData;