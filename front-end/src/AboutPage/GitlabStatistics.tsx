import { Typography, Box, Divider } from '@mui/material/';
import { ReactComponent as CommitIcon } from './icons/commit.svg'
import { ReactComponent as IssueIcon } from './icons/issue.svg'
import { ReactComponent as TestIcon } from './icons/check.svg'
import { ReactComponent as ClosedIcon } from './icons/closed.svg'
import { ReactComponent as OpenIcon } from './icons/open.svg'

// Inspired by: https://gitlab.com/bevos-crew/bevos-course-guide/-/tree/development

function GitlabStatistics(props: any) {
    const { numCommits, numOpenIssues, numClosedIssues, numAuthoredIssues, numTests } = props;

    return (
        <div>
            <Box sx={{
                display: "flex", 
                flexDirection: "row", 
                justifyContent: "space-around", 
                flexWrap: "nowrap",
                borderTop: "solid 1px #A9A9A9",
                paddingTop: "10px",
                width: "100%",
                }}>

                <Box sx={{ 
                    width: "33%",
                    height: "100%",
                    display: "flex", 
                    flexDirection: "column", 
                    flexWrap: "nowrap", 
                    alignItems: "center",
                    flexGrow: 0,
                    }}>
                    <Typography variant="h6" style={{ fontSize: 12, color: "#A9A9A9" }}>Commits</Typography>
                    <CommitIcon fill={"#A9A9A9"} style={{ height: 12 * 2, width: 12 * 2 }} />
                    <Typography variant="h6" style={{ fontSize: 12 + 8, color: "#A9A9A9" }}>{numCommits}</Typography> 
                </Box>
                <Divider orientation="vertical" flexItem />
                <Box sx={{ 
                    width: "33%",
                    height: "100%",
                    display: "flex", 
                    flexDirection: "column", 
                    flexWrap: "nowrap", 
                    alignItems: "center",
                    flexGrow: 0,
                    }}>
                    <Typography variant="h6" style={{ fontSize: 12, color: "#A9A9A9" }}>Issues</Typography>
                    <IssueIcon fill={"#A9A9A9"} style={{ height: 12 * 2, width: 12 * 2 }} />
                    <Typography variant="h6" style={{ fontSize: 12 + 8, color: "#A9A9A9" }}>{numAuthoredIssues}</Typography> 
                </Box>
                <Divider orientation="vertical" flexItem />
                <Box sx={{ 
                    width: "33%",
                    height: "100%",
                    display: "flex", 
                    flexDirection: "column", 
                    flexWrap: "nowrap", 
                    alignItems: "center",
                    flexGrow: 0,
                    }}>
                    <Typography variant="h6" style={{ fontSize: 12, color: "#A9A9A9" }}>Open</Typography>
                    <OpenIcon fill={"#A9A9A9"} style={{ height: 12 * 2, width: 12 * 2 }} />
                    <Typography variant="h6" style={{ fontSize: 12 + 8, color: "#A9A9A9" }}>{numOpenIssues}</Typography> 
                </Box>
                <Divider orientation="vertical" flexItem />
                <Box sx={{ 
                    width: "33%",
                    height: "100%",
                    display: "flex", 
                    flexDirection: "column", 
                    flexWrap: "nowrap", 
                    alignItems: "center",
                    flexGrow: 0,
                    }}>
                    <Typography variant="h6" style={{ fontSize: 12, color: "#A9A9A9" }}>Closed</Typography>
                    <ClosedIcon fill={"#A9A9A9"} style={{ height: 12 * 2, width: 12 * 2 }} />
                    <Typography variant="h6" style={{ fontSize: 12 + 8, color: "#A9A9A9" }}>{numClosedIssues}</Typography> 
                </Box>
                <Divider orientation="vertical" flexItem />
                <Box sx={{ 
                    width: "33%",
                    height: "100%",
                    display: "flex", 
                    flexDirection: "column", 
                    flexWrap: "nowrap", 
                    alignItems: "center",
                    flexGrow: 0,
                    }}>
                    <Typography variant="h6" style={{ fontSize: 12, color: "#A9A9A9" }}>Tests</Typography>
                    <TestIcon fill={"#A9A9A9"} style={{ height: 12 * 2, width: 12 * 2 }} />
                    <Typography variant="h6" style={{ fontSize: 12 + 8, color: "#A9A9A9" }}>{numTests}</Typography> 
                </Box>
            </Box>
        </div>
    );
}

export default GitlabStatistics;
