import React from 'react';
import { Card, CardContent, Typography, CardActionArea } from '@mui/material/';

function GenericCard(props: any) {
    const { image, title, subtitle, link } = props;

    return (
        <CardActionArea href={link} >
            <Card sx={{ width: 300}}>
                <CardContent>
                    <img
                    src={image}
                    alt={title}
                    style={{ width: 200, height: 200 }}
                    />
                    <Typography variant="h5" component="div">
                        {title}
                    </Typography>
                    <Typography variant="subtitle2" component="div">
                        {subtitle}
                    </Typography>
                </CardContent>
            </Card>
        </CardActionArea>
    );
}

export default GenericCard;