import React from 'react';
import { Link } from 'react-router-dom';
import InfoIcon from '@mui/icons-material/Info';
import HomeIcon from '@mui/icons-material/Home';
import PersonIcon from '@mui/icons-material/Person';
import LocationCityIcon from '@mui/icons-material/LocationCity';
import EventSeatIcon from '@mui/icons-material/EventSeat';
import AutoGraphIcon from '@mui/icons-material/AutoGraph';
import BarChartIcon from '@mui/icons-material/BarChart';
import { makeStyles, AppBar, Box, Toolbar, Button, Divider } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
	link: {
	  color: "white",
	  fontSize: "20px",
	  textTransform: 'none'
	},
	divider: {
		background: "white",
	},
  }));

function NavBar() {
	const classes = useStyles();

	return (
		<Box sx={{ flexGrow: 1 }}>
		<AppBar position="static">
			<Toolbar>
			<Link to="/" style={{ textDecoration: 'none' }}>
				<Button className={classes.link}>
					<HomeIcon className={classes.link} />
					Home
				</Button>
			</Link>
			&nbsp;
			<Link to="/about" style={{ textDecoration: 'none' }}>
				<Button className={classes.link}>
					<InfoIcon className={classes.link}/>
					About
				</Button>
			</Link>
			&nbsp;
			<Link to="/artists" style={{ textDecoration: 'none' }}>
				<Button className={classes.link}>
					<PersonIcon className={classes.link}/>
					Artists
				</Button>
			</Link>
			&nbsp;
			<Link to="/cities" style={{ textDecoration: 'none' }}>
				<Button className={classes.link}>
					<LocationCityIcon className={classes.link}/>
					Cities
				</Button>
			</Link>
			&nbsp;
			<Link to="/events" style={{ textDecoration: 'none' }}>
				<Button className={classes.link}>
					<EventSeatIcon className={classes.link}/>
					Events
				</Button>
			</Link>
			&nbsp;
			<Link to="/visualizations" style={{ textDecoration: 'none' }}>
				<Button className={classes.link}>
					<AutoGraphIcon className={classes.link}/>
					Visualizations
				</Button>
			</Link>
			&nbsp;
			<Link to="/providervisualizations" style={{ textDecoration: 'none' }}>
				<Button className={classes.link}>
					<BarChartIcon className={classes.link}/>
					Provider Visualizations
				</Button>
			</Link>
			</Toolbar>
		</AppBar>
		</Box>
	);
}

export default NavBar;