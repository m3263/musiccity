import React from 'react';
import { render, screen } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import ReactDOM from 'react-dom';
import About from '../AboutPage/About';
import App from '../App'
import Splash from '../Splash';
import NavBar from '../shared_components/NavBar';
import Artists from '../Artists/Artists';
import Artist from '../Artists/Artist';
import theme from '../Theme'
import Cities from '../Cities/Cities';
import City from '../Cities/City';
import Events from '../Events/Events';
import EventsModel from '../Events/EventsModel';
import Search from '../Search'


describe("Render Basic Components", () => {
    // Test 1
    test ('App renders without crashing', ()=> {
        <BrowserRouter>
            render(<App />);
            expect(screen.getByText('(/music/i)')).toBeInTheDocument();
        </BrowserRouter>
    });

    // Test 2
    test('Splash renders without crashing', () => {
        <BrowserRouter>
            render(<Splash />);
            expect(screen.getByText('(/music/i)')).toBeInTheDocument();
        </BrowserRouter>
    });

    // Test 3
    test('About renders without crashing', () => {
        <BrowserRouter>
            render(<About />);
            expect(screen.getByText('(/about/i)')).toBeInTheDocument();
        </BrowserRouter>
    });

    // Test 4
    test('NavBar renders without crashing', () => {
        <BrowserRouter>
            render(<NavBar />);
            expect(screen.getByText('(/home/i)')).toBeInTheDocument();
        </BrowserRouter>
    });

    // Test 5
    test('Event instance page renders without crashing', () => {
        <BrowserRouter>
            render(<Events id="1023104702" />);
            expect(screen.getByText('(/ed/i)')).toBeInTheDocument();
        </BrowserRouter>
    });

    // Test 6
    test('Event model page renders without crashing', () => {
        <BrowserRouter>
            render(<EventsModel />);
            expect(screen.getByText('(/page/i)')).toBeInTheDocument();
        </BrowserRouter>
    });

    // Test 7
    test('Artist instance page renders without crashing', () => {
        <BrowserRouter>
            render(<Artist id="6eUKZXaKkcviH0Ku9w2n3V" />);
            expect(screen.getByText('(/ed/i)')).toBeInTheDocument();
        </BrowserRouter>
    });

    // Test 8
    test('Artist model page renders without crashing', () => {
        <BrowserRouter>
            render(<Artists />);
            expect(screen.getByText('(/page/i)')).toBeInTheDocument();
        </BrowserRouter>
    });

    // Test 9
    test('City model page renders without crashing', () => {
        <BrowserRouter>
            render(<Cities />);
            expect(screen.getByText('(/page/i)')).toBeInTheDocument();
        </BrowserRouter>
    });

    // Test 10
    test('City instance page renders without crashing', () => {
        <BrowserRouter>
            render(<City id="Q100" />);
            expect(screen.getByText('(/ed/i)')).toBeInTheDocument();
        </BrowserRouter>
    });

    // Test 11
    test('Search instance page renders without crashing', () => {
        <BrowserRouter>
            render(<Search />);
            expect(screen.getByText('(/ed/i)')).toBeInTheDocument();
        </BrowserRouter>
    });

    // Test 12
    test('City search working', () => {
        <BrowserRouter>
            render(<Cities search="dallas" />);
            expect(screen.getByText('(/Texas/i)')).toBeInTheDocument();
        </BrowserRouter>
    });

    // Test 13
    test('Artist search working', () => {
        <BrowserRouter>
            render(<Artists search="sheeran" />);
            expect(screen.getByText('(/ed sheeran/i)')).toBeInTheDocument();
        </BrowserRouter>
    });

    // Test 14
    test('Event search working', () => {
        <BrowserRouter>
            render(<EventsModel search="eagles" />);
            expect(screen.getByText('(/chase center/i)')).toBeInTheDocument();
        </BrowserRouter>
    });

    // Test 15
    test('Search all working', () => {
        <BrowserRouter>
            render(<Search search="cat" />);
            expect(screen.getByText('(/Elton/i)')).toBeInTheDocument();
            expect(screen.getByText('(/Tecate/i)')).toBeInTheDocument();
            expect(screen.getByText('(/Catalonia/i)')).toBeInTheDocument();
        </BrowserRouter>
    });

    // Test 16
    test('Event sort working', () => {
        <BrowserRouter>
            render(<EventsModel sort="city" />);
            expect(screen.getByText('(/Alpharetta/i)')).toBeInTheDocument();
        </BrowserRouter>
    });

    // Test 17
    test('City sort working', () => {
        <BrowserRouter>
            render(<Cities sort="population" />);
            expect(screen.getByText('(/Alpharetta/i)')).toBeInTheDocument();
        </BrowserRouter>
    });

    // Test 18
    test('Artists sort working', () => {
        <BrowserRouter>
            render(<Artists sort="popularity" />);
            expect(screen.getByText('(/CNCO/i)')).toBeInTheDocument();
        </BrowserRouter>
    });

    // Test 18
    test('Artists filter working', () => {
        <BrowserRouter>
            render(<Artists sort="popularity" genre="canadian pop"/>);
            expect(screen.getByText('(/Weeknd/i)')).toBeInTheDocument();
        </BrowserRouter>
    });
});