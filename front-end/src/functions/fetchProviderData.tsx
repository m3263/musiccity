const axios = require('axios');

function getHighProteinIngredients(count) {
    return axios.get('https://api.broaderhorizons.me/api/ingredients', {
        headers: {
        'Content-Type': 'application/json'
        },
        params: {
            "page": 1,
            "perPage": count,
            "protein": 'high',
            "sort": 'cost'
        }
      })
    .then(response => {
        return response['data']['result']
    })
    .catch(error => console.log(error))
}

function getRecipies() {
    return axios.get('https://api.broaderhorizons.me/api/recipes', {
        headers: {
        'Content-Type': 'application/json'
        },
        params: {
            "perPage": 2691
        }
      })
    .then(response => {
        return response['data']['result']
    })
    .catch(error => console.log(error))
}

function getCuisines() {
    return axios.get('https://api.broaderhorizons.me/api/cuisines', {
        headers: {
        'Content-Type': 'application/json'
        },
        params: {
            "perPage": 157
        }
      })
    .then(response => {
        return response['data']['result']
    })
    .catch(error => console.log(error))
}

export { getHighProteinIngredients, getRecipies, getCuisines };