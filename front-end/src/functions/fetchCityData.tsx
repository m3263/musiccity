const axios = require('axios');

async function getCities(page, step, search, states, countries, sort, order) {
    var params = {"step": step, "page": page + 1, "sort": sort, "order": order, "search": search, "state": states, "country": countries}
    try {
        const response = await axios.get(process.env.REACT_APP_BACKEND_URL + "/cities" , {
            headers: {
            'Content-Type': 'application/json'
            },
            params: params
        })
        return response['data'];
    }
    catch (err) {
        // Handle Error Here
        console.error(err);
    }
}

async function getCityData(id) {
    try {
        const response = await axios.get(process.env.REACT_APP_BACKEND_URL + "/cities/" + id, {
            headers: {
            'Content-Type': 'application/json'
            }
        })
        return response;
    }
    catch (err) {
        // Handle Error Here
        console.error(err);
    }
}


export { getCities, getCityData };