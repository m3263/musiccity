const axios = require('axios');

const url = process.env.REACT_APP_BACKEND_URL

async function getEvents(currPage: any, pageSize: any) {
    var params = {};
    params["pageSize"] = pageSize;
    params["currPage"] = currPage;
    params["sort"] = "time";
    params["search"] = "";
    try {
        const response = await axios.get(url + "/events",
            {
            headers: {
                'Content-Type': 'application/json'
            },
                params: params
        })
        return response;
    }
    catch (err) {
        // Handle Error Here
        console.error(err);
    }
}

async function getEventsSearch(currPage: any, pageSize: any, searchBy: any, search: any, order: any) {
    var params = {};
    if (pageSize > 0 && currPage > 0) {
        params["pageSize"] = pageSize
        params["currPage"] = currPage
    }
    params["pageSize"] = pageSize
    params["currPage"] = currPage
    if (searchBy === "featured") {
        params["featured"] = search;
    }
    else if (searchBy === "venue") {
        params["venue"] = search;
    }
    else if (searchBy === "city") {
        params["city"] = search;
    }
    else if (searchBy === "time") {
        params["time"] = search;
    }
    else if (searchBy === "country") {
        params["country"] = search;
    }
    params["search"] = search;
    params["sort"] = searchBy;
    params["order"] = order;
    return axios.get(url + "/events",
        {
            headers: {
                'Content-Type': 'application/json'
            },
            params: params
        })
        .then(response => {
            return response;
        })
        .catch(error => { console.log(error); console.log(params) })
}

async function getEventData(id) {
    try {
        const response = await axios.get(url + "/events/" + id, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        return response;
    }
    catch (err) {
        // Handle Error Here
        console.error(err);
    }
}

async function getEventLinks(id) {
    try {
        const response = await axios.get(url + "/events/" + id + "/links", {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        return response;
    }
    catch (err) {
        // Handle Error Here
        console.error(err);
    }
}

export { getEvents, getEventData, getEventLinks, getEventsSearch };
