const axios = require('axios');

function getFollowerData(limit) {
    return axios.get(process.env.REACT_APP_BACKEND_URL + "/artist/followers", {
        headers: {
        'Content-Type': 'application/json'
        },
        params: {
            "limit": limit
        }
      })
    .then(response => {
        return response['data']
    })
    .catch(error => console.log(error))
}

function getCityEvent() {
    return axios.get(process.env.REACT_APP_BACKEND_URL + "/citiesLinks", {
        headers: {
        'Content-Type': 'application/json'
        }
      })
    .then(response => {
        return response['data']
    })
    .catch(error => console.log(error))
}

export { getFollowerData, getCityEvent };