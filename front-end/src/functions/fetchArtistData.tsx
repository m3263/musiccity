const axios = require('axios');

function getArtistData(id) {
    return axios.get(process.env.REACT_APP_BACKEND_URL + "/artist/" + id, {
        headers: {
        'Content-Type': 'application/json'
        }
      })
    .then(response => {
        return response['data']
    })
    .catch(error => console.log(error))
}

function getArtists(search, itemsPerPage, page, sort, sortOrder, genre, related) {
    var params = {"step": itemsPerPage, "page": page + 1, "sort": sort, "order": sortOrder, "search": search, "genre": genre.join(','), "related": related.join(',')};
    return axios.get(process.env.REACT_APP_BACKEND_URL + "/artists", 
    {
        headers: {
            'Content-Type': 'application/json'
        },
        params: params
      })
    .then(response => {
        return response['data']
    })
    .catch(error => console.log(error))
}

function getArtistRelationships(id) {
    return axios.get(process.env.REACT_APP_BACKEND_URL + "/artistRel/" + id,
    {
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => {
        return response['data']
    })
    .catch(error => console.log(error))
}



export { getArtistData, getArtists, getArtistRelationships };