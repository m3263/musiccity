const axios = require('axios');

function getAllData(query) {
    return axios.get(process.env.REACT_APP_BACKEND_URL + "/search", {
        headers: {
        'Content-Type': 'application/json'
        },
        params: {
            "query": query,
            "page": 1,
            "step": 9
        }
      })
    .then(response => {
        return response['data']
    })
    .catch(error => console.log(error))
}

export { getAllData };