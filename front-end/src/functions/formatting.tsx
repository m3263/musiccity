function FtoC(temp) {
    return (temp - 32) * (5/9)
}

function formatStr(str, max) {
    if (str.length < max) {
        return str
    }
    str = str.slice(0, max - 3)
    let index = str.lastIndexOf(' ')
    if (index < 0){
        return str.slice(0, max - 3) + "..."
    }
    return str.slice(0, index) + "..."
}

function formatNumber(num) {
    if (num > 999999) {
        return Math.floor(num/1000000).toString() + "M"
    }
    if (num > 99999) {
        return Math.floor(num/1000).toString() + "K"
    }
    return num.toString()
}

function formatDate(str) {
    const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var parse = Date.parse(str)
    var date = new Date(parse);

    let month = months[date.getMonth()];

    return month + " " + date.getDate() + ", " + date.getFullYear()
}

function formatTime(str) {
    const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var parse = Date.parse(str)
    var date = new Date(parse);

    let month = months[date.getMonth()];
    let minutes = date.getMinutes()
    var minStr = minutes.toString()
    if (minutes < 10) {
        minStr += "0"
    }

    return month + " " + date.getDate() + ", " + date.getFullYear() + " " + date.getHours() + ":" + minStr
}

export { formatNumber, formatDate, formatTime, formatStr, FtoC }