import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { ThemeProvider } from '@material-ui/core/styles'
import './App.css';
import About from './AboutPage/About';
import Splash from './Splash';
import NavBar from './shared_components/NavBar';
import Artists from './Artists/Artists';
import theme from './Theme'
import Artist from './Artists/Artist';
import Cities from './Cities/Cities';
import City from './Cities/City';
import Events from './Events/Events';
import EventsModel from './Events/EventsModel';
import Visualizations from './Visualizations/Visualizations';
import ProviderVisualizations from './Visualizations/ProviderVisualizations';
import Search from './Search';

function App() {
  return (
    <div>
      <BrowserRouter>
        <ThemeProvider theme={theme}>
        <NavBar />
        <Switch>
          <Route path="/about" component={About}/>
          <Route path="/artists/:id" component={Artist}/>
          <Route path="/artists" component={Artists}/>
          <Route path="/events/:id" component={Events}/>
          <Route path="/events" component={EventsModel}/>
          <Route path="/cities/:id" component={City}/>
          <Route path="/cities" component={Cities}/>
          <Route path="/search" component={Search}/>
          <Route path="/visualizations" component={Visualizations}/>
          <Route path="/providervisualizations" component={ProviderVisualizations}/>
          <Route exact path="/" component={Splash}/>
        </Switch>
        </ThemeProvider>
        
      </BrowserRouter>
    </div>
  );
}

export default App;
